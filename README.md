* Install Android Studio
* Install Android 2.2, 2.3.3, 4.2.2 + Google APIs for each of them.
* Install Extras / Google Repository, Android Support Repository.

## Generate signing apk

Locate your signing certificate.

Then add the following environment vars:

    $ export ANDROID_KEYSTORE_PASSWORD=your_keystore_password
    $ export ANDROID_KEY_PASSWORD=your_key_password

If you also use Android Studio on Mac OS X edit file `/etc/launchd.conf` by adding the following lines:

    setenv ANDROID_KEYSTORE_PASSWORD pass
    setenv ANDROID_KEY_PASSWORD pass

For details see http://stackoverflow.com/questions/135688/setting-environment-variables-in-os-x/588442#588442.

To generate the signed apk go to the project root and run:

    $ ./gradlew assembleRelease

When the build completes successfully you can find your signed apk at
`<project_root>/DrugStoreLocator/build/apk/DrugStoreLocator-release.apk`.
