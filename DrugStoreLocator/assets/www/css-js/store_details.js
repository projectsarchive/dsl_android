var mustacheTemplate = '<h2>{{store.name}}</h2><div class="address">{{store.address}}</div>' +
	'<h3>{{#translate}}working_hours{{/translate}}</h3>' +
	'<ul id="worktime">' +
	'{{#workingDays}}<li {{#currentDay}}class="today"{{/currentDay}}><span>{{dayId}}</span><span>{{#hours}}с {{from}} до {{to}}&nbsp;&nbsp;{{/hours}}</span></li>{{/workingDays}}' +
	'</ul>';

var documentReady = false;
var content;
var onReadyFunctions = new Array();

$(function () {
	documentReady = true;
	content = $("#wrapper");
	if (onReadyFunctions.length) {
		onReadyFunctions.forEach(function (obj) {
			obj();
		});
	}
});

// >>>>>>>>>>>>>>>>>

var debug = false;

// <<<<<<<<<<<<<<<<<

function runOnReady(func) {
	if (documentReady) {
		func();
	} else {
		onReadyFunctions.push(func);
	}
}
function fillData(json) {
	var html = "";
	json['translate'] = function () {
		return function (text) {
			return translate(text);
		}
	};

	html += Mustache.to_html(mustacheTemplate, json);
	runOnReady(function () {
		content.html(html);
	});
}

function development() {
	if (!debug) {
		return;
	}
	j = {"store":{"address":"Салавата Юлаева, 3", "groupId":"2111599196243115", "rubrics":"", "id":"2111590608515368", "name":"Алвик, сеть аптек", "point":"55188639,61277165", "city":"1", "alvikId":"0"}, "workingDays":[
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Пн"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Вт"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Ср"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Чт"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Пт"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Сб"},
		{"hours":[
			{"from":"09:00", "to":"20:00"}
		], "dayId":"Вс"}
	]};

	JSCommon = {
		'getLanguage':function () {
			return 'ru';
		}
	};
	fillData(j);
	runOnReady(function () {
		$("body").css("background-color", "black");
	});
}