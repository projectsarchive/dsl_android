var translations = {
	'nothing_found':{
		'default':'Nothing found.',
		'ru':'Ничего не найдено :('
	},
	'working_hours':{
		'default':'Working hours:',
		'ru':'Часы работы:'
	},
	'store_address':{
		'default':'Store address',
		'ru':'Адрес'
	}
};

function translate(message) {
	var lang = 'default';
	if (typeof JSCommon != 'undefined') {
		lang = JSCommon.getLanguage();
		console.log("Getting language:" + lang);
	}
	if (!translations.hasOwnProperty(message)) {
		return message;
	}
	var el = translations[message];
	if (!el.hasOwnProperty(lang)) {
		if (el.hasOwnProperty('default')) {
			return el['default'];
		} else {
			return message;
		}
	} else {
		return el[lang];
	}
}