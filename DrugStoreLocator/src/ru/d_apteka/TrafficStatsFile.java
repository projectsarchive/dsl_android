package ru.d_apteka;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import android.util.Log;

/**
 * Class for getting the traffic statistics from files
 * @author stas
 * @see <a href="https://www.codeaurora.org/git/projects/hisense-ts7008/repository/revisions/7c9a6fab23094f42ac73d05a83b4cc3d8c10a31d/entry/Gingerbread/frameworks/base/core/jni/android_net_TrafficStats.cpp">Gingerbread sources</a>
 * @see <a href="http://stackoverflow.com/questions/4029186/can-someone-explain-how-trafficstats-works-its-magic-in-the-android-os/9472571#9472571">Discussion on porting the sources</a>
 *
 */
public class TrafficStatsFile {

	private static final String mobileRxFile_1 = "/sys/class/net/rmnet0/statistics/rx_bytes";
	private static final String mobileRxFile_2 = "/sys/class/net/ppp0/statistics/rx_bytes";
	private static final String mobileTxFile_1 = "/sys/class/net/rmnet0/statistics/tx_bytes";
	private static final String mobileTxFile_2 = "/sys/class/net/ppp0/statistics/tx_bytes";

	private static final String LOGGING_TAG = TrafficStatsFile.class
			.getSimpleName();

	public long getMobileRxBytes() {
		return tryBoth(mobileRxFile_1, mobileRxFile_2);
	}

	public long getMobileTxBytes() {
		return tryBoth(mobileTxFile_1, mobileTxFile_2);
	}

	// Return the number from the first file which exists and contains data
	private static long tryBoth(String a, String b) {
		long num = readNumber(a);
		return num >= 0 ? num : readNumber(b);
	}

	// Returns an ASCII decimal number read from the specified file, -1 on
	// error.
	private static long readNumber(String filename) {
		try {
			RandomAccessFile f = new RandomAccessFile(filename, "r");
			try {
				Log.d(LOGGING_TAG, "f.length() = " + f.length());
				String contents = f.readLine();
				if (!contents.equals("") && contents != null) {
					try {
						return Long.parseLong(contents);
					} catch (NumberFormatException nfex) {
						Log.w(LOGGING_TAG, "File contents are not numeric: "
								+ filename);
					}
				} else {
					Log.w(LOGGING_TAG, "File contents are empty: " + filename);
				}
			} catch (FileNotFoundException fnfex) {
				Log.w(LOGGING_TAG, "File not found: " + filename, fnfex);
			} catch (IOException ioex) {
				Log.w(LOGGING_TAG, "IOException: " + filename, ioex);
			}
		} catch (FileNotFoundException ffe) {
			//Log.w(LOGGING_TAG, "File not found: " + filename, ffe);
		}
		return -1;
	}
	
	// Per-UID stats require reading from a constructed filename.
	
	static long getUidRxBytes(int uid) {
	    String filename;
	    filename = String.format("/proc/uid_stat/%d/tcp_rcv", uid);
	    return readNumber(filename);
	}
	
	static long getUidTxBytes(int uid) {
	    String filename;
	    filename = String.format("/proc/uid_stat/%d/tcp_snd", uid);
	    return readNumber(filename);
	}

}