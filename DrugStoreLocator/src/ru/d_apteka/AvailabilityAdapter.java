/**
 * 
 */
package ru.d_apteka;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask.Status;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Adapter for fetching {@link MedicineItem} availability in {@link Store}s from
 * the server.
 * 
 * @author stas
 */
public class AvailabilityAdapter
		extends
		DynamicViewAdapter<ItemInStore, GetAvailabilityTask, ArrayList<ItemInStore>> {

	private GetAvailabilityTask currentTask;
	private ArrayList<ItemInStore> items = new ArrayList<ItemInStore>();
	private int curPage = 0;
	private MedicineItem item;
	private boolean loadingFinished = false;
	private Context context;
	private int mAdditionalFieldId = 0;
	private int mPriceFieldId = 0;
	private int mDistanceFieldId = 0;
	private SmartLocationTeller mSmartLocationListener;
	private MyApp app;

	public AvailabilityAdapter(Context context, SmartLocationTeller smartLocationListener, MedicineItem item,
			int textViewResourceId, int loadingViewId,
			ArrayList<ItemInStore> items, int page) {
		super(context, textViewResourceId, loadingViewId);
		init(context, smartLocationListener, item, items, page);
	}

	public AvailabilityAdapter(Context context, SmartLocationTeller smartLocationListener, MedicineItem item,
			int resource, int textViewResourceId, int additionalFieldId,
			int priceFieldId, int distanceFieldId, int loadingViewId,
			ArrayList<ItemInStore> items, int page) {
		super(context, resource, textViewResourceId, loadingViewId);
		mAdditionalFieldId = additionalFieldId;
		mPriceFieldId = priceFieldId;
		mDistanceFieldId = distanceFieldId;
		init(context, smartLocationListener, item, items, page);
	}

	private void init(Context context, SmartLocationTeller smartLocationListener, MedicineItem item,
			ArrayList<ItemInStore> items, int page) {
		this.item = item;
		this.context = context;
		this.items = items;
		this.count = items.size();
		this.curPage = page;
		this.mSmartLocationListener = smartLocationListener;
		app = (MyApp) this.context.getApplicationContext();
	}

	/**
	 * {@inheritDoc}
	 */
	public void onTaskComplete(ArrayList<ItemInStore> result) {
		int size = 0;
		if (result != null) {
			size = result.size();
			items.addAll(result);
			count += size;
		}
		loadingFinished = size == 0;
		notifyDataSetChanged();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#getData()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void getData() {
		if (currentTask == null || currentTask.getStatus() != Status.RUNNING) {
			GetAvailabilityTask task = new GetAvailabilityTask(app, mSmartLocationListener);
			task.setOnTaskCompleteListener(this);
			DynamicTaskParams<MedicineItem> params = new DynamicTaskParams<MedicineItem>(
					item, curPage);
			curPage++;
			task.execute(params);
			currentTask = task;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#getItem(int)
	 */
	@Override
	public ItemInStore getItem(int position) {
		if (position < count)
			return items.get(position);
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#loadingFinished()
	 */
	@Override
	public boolean loadingFinished() {
		return loadingFinished;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#onCreatingViewFromResource(android.view.View,
	 *      java.lang.Object)
	 */
	@Override
	protected View onCreatingViewFromResource(View view, ItemInStore item) {
		if (mAdditionalFieldId != 0) {
			TextView text;
			try {
				text = (TextView) view.findViewById(mAdditionalFieldId);
			} catch (ClassCastException e) {
				Log.e("AvailabilityAdapter",
						"You must supply an additional field resource ID for a TextView");
				throw new IllegalStateException(
						"AvailabilityAdapter requires the additional field resource ID to be a TextView",
						e);
			}
			text.setText(item.getStore().getName());
			text.setTypeface(mTypeface);
		}
		DecimalFormat twoDecimals = new DecimalFormat("###,##0.00");
		if (mPriceFieldId != 0) {
			TextView price;
			try {
				price = (TextView) view.findViewById(mPriceFieldId);
			} catch (ClassCastException e) {
				Log.e("AvailabilityAdapter",
						"You must supply a price field resource ID for a TextView");
				throw new IllegalStateException(
						"AvailabilityAdapter requires the price field resource ID to be a TextView",
						e);
			}
			String priceString = twoDecimals.format(item.getPrice());
			price.setText(priceString);
			price.setTypeface(mTypeface);
		}
		if (mDistanceFieldId != 0) {
			String dist = twoDecimals.format(item.getDistance());
			TextView distance;
			try {
				distance = (TextView) view.findViewById(mDistanceFieldId);
			} catch (ClassCastException e) {
				Log.e("AvailabilityAdapter",
						"You must supply a distance field resource ID for a TextView");
				throw new IllegalStateException(
						"AvailabilityAdapter requires the distance field resource ID to be a TextView",
						e);
			}
			distance.setText(dist + " km");
			distance.setTypeface(mTypeface);
		}
		return view;
	}

	/**
	 * @return the items
	 */
	public ArrayList<ItemInStore> getItems() {
		return items;
	}

	public MedicineItem getRootItem() {
		return item;
	}
	
	/**
	 * @return the curPage
	 */
	public int getCurPage() {
		return curPage;
	}
	
	public void cancelCurrentTask() {
		if(currentTask != null && currentTask.getStatus() == Status.RUNNING) {
			currentTask.cancel(true);
			loadingFinished = false;
		}
	}
}
