package ru.d_apteka;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UsersDatabaseHelper extends SQLiteOpenHelper {
	public static final String DB_NAME = "users.db";
	public static final int DB_VERSION = 1;

	public static final String LISTS_TABLE = "lists";
	public static final String LISTS_NAME = "name";

	public static final String STARRED_ITEMS_TABLE = "starred_items";
	public static final String STARRED_ITEMS_LIST_ID = "list_id";
	public static final String STARRED_ITEMS_ITEM_CODE = "item_code";
	public static final String STARRED_ITEMS_ITEM_NAME = "item_name";

	private final String mCreateListsTable = String.format(
			"CREATE TABLE %s (_id INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)",
			LISTS_TABLE, LISTS_NAME);
	private final String mCreateStarredItemsTable = String
			.format("CREATE TABLE %1$s (_id INTEGER PRIMARY KEY AUTOINCREMENT, %2$s INTEGER NOT NULL, %3$s INTEGER NOT NULL, %5$s TEXT, "
					+ "FOREIGN KEY(%2$s) REFERENCES %4$s(_id) ON DELETE CASCADE);"
					+ " CREATE INDEX list_id_idx on %1$s(%2$s);"
					+ " CREATE INDEX item_code_idx on %1$s(%3$s);",
					STARRED_ITEMS_TABLE, STARRED_ITEMS_LIST_ID,
					STARRED_ITEMS_ITEM_CODE, LISTS_TABLE,
					STARRED_ITEMS_ITEM_NAME);
	private final String mAddDefaultListTemplate = "INSERT INTO %1$s (%2$s) VALUES ('%3$s');";
	private String mAddDefaultListQuery;

	public UsersDatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		mAddDefaultListQuery = String.format(mAddDefaultListTemplate,
				LISTS_TABLE, LISTS_NAME,
				context.getString(R.string.lists_db_default_list_name));
	}

	public ArrayList<ItemsList> getUserLists() {
		ArrayList<ItemsList> result = new ArrayList<ItemsList>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(LISTS_TABLE, new String[] { "_id" }, null, null,
				null, null, null);
		if (c != null) {
			ArrayList<Long> ids = new ArrayList<Long>();
			while (c.moveToNext()) {
				ids.add(c.getLong(0));
			}
			if (!c.isClosed())
				c.close();
			db.close();

			for (Long id : ids) {
				ItemsList list = getListById(id);
				if (list != null) {
					result.add(list);
				}
			}
		}
		return result;
	}

	public boolean saveList(ItemsList list) {
		boolean result = false;
		ContentValues cv = new ContentValues();
		cv.put(LISTS_NAME, list.getName());
		if (getListById(list.getId()) == null) {
			// Create a new list
			SQLiteDatabase db = getWritableDatabase();
			long id = db.insert(LISTS_TABLE, null, cv);
			if (id != -1) {
				result = true;
				list.setId(id);
			}
			db.close();
		} else {
			// Update an existing list
			SQLiteDatabase db = getWritableDatabase();
			result = db.update(LISTS_TABLE, cv, "_id=?",
					new String[] { Long.toString(list.getId()) }) > 0;
			db.close();
		}
		if (result) {
			SQLiteDatabase db = getWritableDatabase();
			db.delete(STARRED_ITEMS_TABLE, STARRED_ITEMS_LIST_ID + "=?",
					new String[] { Long.toString(list.getId()) });
			for (MedicineItem item : list.getItems()) {
				addItemToList(item, list, db);
			}
			db.close();
		}
		return result;
	}

	public boolean removeList(ItemsList list) {
		SQLiteDatabase db = getWritableDatabase();

		int deleted = db.delete(LISTS_TABLE, "_id=?",
				new String[] { Long.toString(list.getId()) });
		boolean result = deleted > 0;

		db.close();

		if (result) {
			ArrayList<ItemsList> lists = getUserLists();
			if (lists.size() == 0) {
				db = getWritableDatabase();
				db.execSQL(mAddDefaultListQuery);
				db.close();
			}
		}

		return result;
	}

	public boolean addItemToList(MedicineItem item, ItemsList list) {
		boolean result = false;
		SQLiteDatabase db = getWritableDatabase();

		result = addItemToList(item, list, db);
		if (result) {
			list.getItems().add(item);
		}

		db.close();
		return result;
	}

	private boolean addItemToList(MedicineItem item, ItemsList list,
			SQLiteDatabase db) {
		boolean result;
		ContentValues cv = new ContentValues();
		if (itemInListIndex(item, list, db, cv) == null) {
			result = db.insert(STARRED_ITEMS_TABLE, null, cv) > 0;
		} else {
			result = false;
		}
		return result;
	}

	public boolean removeItemFromList(MedicineItem item, ItemsList list) {
		boolean result = false;
		SQLiteDatabase db = getWritableDatabase();

		String index = itemInListIndex(item, list, db);
		if (index == null) {
			result = true;
		} else {
			result = db.delete(STARRED_ITEMS_TABLE, "_id=?",
					new String[] { index }) == 1;
		}

		db.close();
		if (result) {
			list.getItems().remove(item);
		}
		return result;
	}

	private String itemInListIndex(MedicineItem item, ItemsList list,
			SQLiteDatabase db) {
		ContentValues cv = new ContentValues();
		return itemInListIndex(item, list, db, cv);
	}

	private String itemInListIndex(MedicineItem item, ItemsList list,
			SQLiteDatabase db, ContentValues cv) {
		String result = null;
		cv.put(STARRED_ITEMS_LIST_ID, list.getId());
		cv.put(STARRED_ITEMS_ITEM_CODE, item.getId());
		cv.put(STARRED_ITEMS_ITEM_NAME, item.getName());

		Cursor c = db.query(STARRED_ITEMS_TABLE, new String[] { "_id" },
				STARRED_ITEMS_ITEM_CODE + "=? AND " + STARRED_ITEMS_LIST_ID
						+ "=?",
				new String[] { cv.get(STARRED_ITEMS_ITEM_CODE).toString(),
						cv.get(STARRED_ITEMS_LIST_ID).toString() }, null, null,
				null);

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			result = c.getString(0);
		}

		if (c != null && !c.isClosed())
			c.close();
		return result;
	}

	public ArrayList<ItemsList> getListsByItemCode(String itemCode) {
		ArrayList<ItemsList> lists = new ArrayList<ItemsList>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(STARRED_ITEMS_TABLE,
				new String[] { STARRED_ITEMS_LIST_ID }, STARRED_ITEMS_ITEM_CODE
						+ "=?", new String[] { itemCode }, null, null, null);
		if (c != null) {
			int listId;
			ItemsList list = null;
			while (c.moveToNext()) {
				listId = c.getInt(0);
				list = getListById(listId);
				if (list != null)
					lists.add(list);
			}
			if (!c.isClosed())
				c.close();
		}
		db.close();
		return lists;
	}

	private ItemsList getListByAttribute(String attributeName,
			Object attributeValue, SQLiteDatabase db) {
		Cursor c = db.query(LISTS_TABLE, new String[] { "_id", LISTS_NAME },
				attributeName + "=?",
				new String[] { attributeValue.toString() }, null, null, null);
		ItemsList list = null;
		if (c != null) {
			if (c.moveToFirst()) {
				list = new ItemsList();
				list.setId(c.getInt(0));
				list.setName(c.getString(1));
				list.setItems(getItemsOfList(list));
			}
			if (!c.isClosed()) {
				c.close();
			}
		}
		return list;
	}

	public ItemsList getListByAttribute(String attributeName,
			Object attributeValue) {
		SQLiteDatabase db = this.getReadableDatabase();
		ItemsList list = getListByAttribute(attributeName, attributeValue, db);
		db.close();
		return list;
	}

	private ItemsList getListById(long listId, SQLiteDatabase db) {
		return getListByAttribute("_id", listId, db);
	}

	public ItemsList getListById(long listId) {
		SQLiteDatabase db = this.getReadableDatabase();
		ItemsList list = getListById(listId, db);
		db.close();
		return list;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(mCreateListsTable);
		db.execSQL(mCreateStarredItemsTable);
		db.execSQL(mAddDefaultListQuery);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	public ArrayList<MedicineItem> getItemsOfList(ItemsList list) {
		SQLiteDatabase db = getReadableDatabase();
		ArrayList<MedicineItem> result = new ArrayList<MedicineItem>();
		Cursor c = db.query(STARRED_ITEMS_TABLE, new String[] {
				STARRED_ITEMS_ITEM_CODE, STARRED_ITEMS_ITEM_NAME },
				STARRED_ITEMS_LIST_ID + "=?",
				new String[] { Long.toString(list.getId()) }, null, null, null);
		if (c != null && c.getCount() > 0) {
			while (c.moveToNext()) {
				result.add(new MedicineItem(c.getInt(0), c.getString(1)));
			}
		}
		if (c != null && !c.isClosed()) {
			c.close();
		}
		db.close();
		return result;
	}


}
