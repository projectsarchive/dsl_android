package ru.d_apteka;

import java.io.Serializable;

/**
 * Represents an item
 * 
 * @author stas
 * 
 */
public class MedicineItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;

	public MedicineItem(Integer id, String name) {
		super();
		setId(id);
		setName(name);
	}

	@Override
	public String toString() {
		return getName();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (!MedicineItem.class.isInstance(o)) {
			return super.equals(o);
		} else {
			return ((MedicineItem) o).getId().equals(getId());
		}
	}
}
