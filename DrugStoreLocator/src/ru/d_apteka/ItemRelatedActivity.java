/**
 * 
 */
package ru.d_apteka;

import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 *
 */
public class ItemRelatedActivity extends MyActivity {

	protected MedicineItem mItem;
	protected Bundle mExtras;
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.MyActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mExtras = getIntent().getExtras();
		if (mExtras == null) {
			Log.e(this.toString(),
					"The Activity cannot be started without parameters. Finishing.");
			finish();
		}

	}

    @Override
    protected String getCurrentItemName() {
        if(mItem != null) {
            return mItem.getName();
        } else {
            return null;
        }
    }

    /* (non-Javadoc)
         * @see ru.d_apteka.MyActivity#setContentView(int)
         */
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		createItems();
	}
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.MyActivity#setContentView(android.view.View)
	 */
	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		createItems();
	}
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.MyActivity#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
	 */
	@Override
	public void setContentView(View view,
			android.view.ViewGroup.LayoutParams params) {
		super.setContentView(view, params);
		createItems();
	}
	
	private void createItems() {
		
		mItem = (MedicineItem) mExtras.getSerializable("item");

		Button showMenuBtn = (Button) findViewById(R.id.showMenu);
		showMenuBtn.setVisibility(View.VISIBLE);
		registerForContextMenu(showMenuBtn);
		showMenuBtn.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mActivity.openContextMenu(v);
			}
		});
		showMenuBtn.setOnLongClickListener(new OnLongClickListener() {
			
			public boolean onLongClick(View v) {
				mActivity.closeContextMenu();
				return false;
			}
		});

		LinearLayout commonTitlebarWrapper = (LinearLayout) findViewById(R.id.common_titlebar_wrapper);
		RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) commonTitlebarWrapper.getLayoutParams();
		params.addRule(RelativeLayout.LEFT_OF, R.id.showMenu);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		commonTitlebarWrapper.setLayoutParams(params);

		final TextView showMenuHintTextView = (TextView) findViewById(R.id.showMenuHint);
		showMenuBtn.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					showMenuHintTextView.setVisibility(View.VISIBLE);
					// hiTextView.startAnimation(new MyScaler(1.0f, 1.0f,
					// 0.0f, 1.0f, 300, hiTextView, false));
					AlphaAnimation animation1 = new AlphaAnimation(0.0f,
							1.0f);
					animation1.setDuration(500);
					animation1.setFillAfter(true);
					showMenuHintTextView.startAnimation(animation1);
				}
				if (event.getAction() == MotionEvent.ACTION_UP) {
					//						hiTextView.setVisibility(View.GONE);
					// hiTextView.startAnimation(new MyScaler(1.0f, 1.0f,
					// 1.0f, 0.0f, 300, hiTextView, true));
					AlphaAnimation animation1 = new AlphaAnimation(1.0f,
							0.0f);
					animation1.setDuration(300);
					animation1.setFillAfter(true);
					animation1.setAnimationListener(new AnimationListener() {

						public void onAnimationStart(Animation animation) {

						}

						public void onAnimationRepeat(Animation animation) {

						}

						public void onAnimationEnd(Animation animation) {
							showMenuHintTextView.setVisibility(View.GONE);
						}
					});
					showMenuHintTextView.startAnimation(animation1);
				}
				return false;
			}
		});

	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public final void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(R.string.item_related_activity_items_actions);
		menu.add(Menu.NONE, 1, 1, R.string.item_related_activity_add_to_list);
		_onCreateContextMenu(menu, v, menuInfo);
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	public final boolean onContextItemSelected(MenuItem item) {
		boolean result = true;
		if(item.getItemId() == 1) {
			ListsManager manager = new ListsManager(mActivity);
			manager.manageListsForItem(mItem);
		} else {
			result = _onContextItemSelected(item);
		}
		return result;
	}

	/**
	 * @param item
	 * @return
	 */
	protected boolean _onContextItemSelected(MenuItem item) {
		return false;
	}

	/**
	 * @param menu
	 * @param v
	 * @param menuInfo
	 */
	protected void _onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		
	}
	
	protected void _onCreateCommonMenu(Menu menu) {
		
	}

	public class MyScaler extends ScaleAnimation {

		private View mView;

		private LayoutParams mLayoutParams;

		private int mMarginBottomFromY, mMarginBottomToY;

		private boolean mVanishAfter = false;

		public MyScaler(float fromX, float toX, float fromY, float toY,
				int duration, View view, boolean vanishAfter) {
			super(fromX, toX, fromY, toY);
			setDuration(duration);
			mView = view;
			mVanishAfter = vanishAfter;
			mLayoutParams = (LayoutParams) view.getLayoutParams();
			int height = mView.getHeight();
			mMarginBottomFromY = (int) (height * fromY)
					+ mLayoutParams.bottomMargin - height;
			// mMarginBottomToY = (int) (0 - ((height * toY) +
			// mLayoutParams.bottomMargin)) - height;
			mMarginBottomToY = (int) ((height * toY) + mLayoutParams.bottomMargin)
					- height;
		}

		@Override
		protected void applyTransformation(float interpolatedTime,
				Transformation t) {
			super.applyTransformation(interpolatedTime, t);
			if (interpolatedTime < 1.0f) {
				int newMarginBottom = mMarginBottomFromY
						+ (int) ((mMarginBottomToY - mMarginBottomFromY) * interpolatedTime);
				mLayoutParams.setMargins(mLayoutParams.leftMargin,
						mLayoutParams.topMargin, mLayoutParams.rightMargin,
						newMarginBottom);
				mView.getParent().requestLayout();
			} else if (mVanishAfter) {
				mView.setVisibility(View.GONE);
			}
		}

	}
}
