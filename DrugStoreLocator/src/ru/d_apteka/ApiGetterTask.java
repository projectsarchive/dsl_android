/**
 * 
 */
package ru.d_apteka;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.widget.Toast;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 *
 */
public abstract class ApiGetterTask<Param, Result> extends AsyncTask<Param, Void, Result> {

	protected OnTaskCompleteListener<Result> mTaskCompleteListener;
	protected MyApp app;
	private IWebReader reader;
	protected boolean internetError = false;
	
	public ApiGetterTask(MyApp app) {
		super();
		this.app = app;
		reader = new WebReader();
	}

	/**
	 * {@inheritDoc}
     *
     * Params can be placed in the path itself. To do this, specify the path like this:
     *
     * http://example.com/api/items/:id
     *
     * And add a param in the params ({@link ru.d_apteka.ApiGetterTask#getUriParams(Object[])}):
     *
     * id => value
	 *
	 */
	@Override
	protected Result doInBackground(
			Param... params) {
		if(!beforeRun(params)) {
			return null;
		}
		String result;
		String path = getPath();
		ArrayList<BasicNameValuePair> paramsUri = getUriParams(params);
        // Use the copy to remove elements in the foreach
        // @see http://stackoverflow.com/questions/8189466/java-util-concurrentmodificationexception
        ArrayList<BasicNameValuePair> paramsUriIterate = new ArrayList<BasicNameValuePair>(paramsUri);

        for(BasicNameValuePair param : paramsUriIterate) {
            if(path.contains(":" + param.getName())) {
                path = path.replace(":" + param.getName(), param.getValue());
                paramsUri.remove(param);
            }
        }
		
		try {
			result = reader.readWebResource(path, paramsUri);
		} catch (IOException e1) {
			this.internetError = true;
			return null;
		}

		return processOutput(result, params);
	}

	/**
	 * Any actions before executing the task. All the checks should be done here.
	 * It's executed before {@link #getUriParams(Object...)} and {@link #processOutput(String, Object...)}
	 * @return If the task should be executed.
	 */
	protected boolean beforeRun(Param... params) {
		return true;
	}

	abstract protected String getPath();

	abstract protected ArrayList<BasicNameValuePair> getUriParams(Param... params);

	abstract protected Result processOutput(String result, Param...params);
	
	/**
	 * @param mTaskCompleteListener the mTaskCompleteListener to set
	 */
	public void setOnTaskCompleteListener(
			OnTaskCompleteListener<Result> mTaskCompleteListener) {
		this.mTaskCompleteListener = mTaskCompleteListener;
	}
	
	/* 
	 * {@inheritDoc}
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		if (mTaskCompleteListener != null)
			mTaskCompleteListener.onTaskComplete(result);
		this.onFinish();
	}

    protected int getInternetErrorMessage() {
        return R.string.internet_error_common;
    }
	
	private void onFinish() {
		if(this.internetError) {
			try {
				Toast toast = Toast.makeText(app.getApplicationContext(), this.getInternetErrorMessage(), Toast.LENGTH_LONG);
				toast.show();
			} catch(Exception e) {}
		}
	}
}
