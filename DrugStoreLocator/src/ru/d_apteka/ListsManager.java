package ru.d_apteka;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

public class ListsManager {
	private Context mContext;

	public ListsManager(Context context) {
		mContext = context;
	}

	public void manageListsForItem(final MedicineItem item) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.lists_manager_dialog, null);
		ListView listView = (ListView) layout
				.findViewById(R.id.lists_manager_lists);
		ListsCheckboxAdapter adapter = new ListsCheckboxAdapter(mContext, item);
		listView.setAdapter(adapter);
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
				.setView(layout);
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
