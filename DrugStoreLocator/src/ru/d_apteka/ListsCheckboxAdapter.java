package ru.d_apteka;

import ru.d_apteka.ListsEditor.ListsEditedListener;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class ListsCheckboxAdapter extends ListsListAdapter {
	private MedicineItem mItem;
	private UsersDatabaseHelper mHelper;

	public ListsCheckboxAdapter(Context context, MedicineItem item) {
		super(context);
		mItem = item;
		mHelper = new UsersDatabaseHelper(mContext);
		mShowAddNew = true;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			view = mInflater.inflate(R.layout.lists_manager_lists_item, null);
			holder = new ViewHolder();
			holder.item = (CheckBox) view
					.findViewById(R.id.lists_manager_lists_item_checkbox);
			holder.itemView = (TextView) view
					.findViewById(R.id.lists_manager_lists_item_textview);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		if (position < mList.size()) {
			final ItemsList list = mList.get(position);
			boolean active = list.hasItem(mItem);
			holder.item.setText(list.getName());
			holder.item.setOnCheckedChangeListener(null);
			holder.item.setChecked(active);
			holder.itemView.setVisibility(View.GONE);
			holder.item.setVisibility(View.VISIBLE);
			holder.item.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(
						CompoundButton buttonView, boolean isChecked) {
					if(isChecked) {
						mHelper.addItemToList(mItem, list);
					} else {
						mHelper.removeItemFromList(mItem, list);
					}
				}
			});
		} else {
			holder.item.setOnCheckedChangeListener(null);
			holder.item.setVisibility(View.GONE);
			holder.itemView.setVisibility(View.VISIBLE);
			holder.itemView.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					ListsEditor adder = new ListsEditor(mContext);
					adder.setListsAddedListener(new ListsEditedListener() {
						
						@Override
						public void onListEdited(ItemsList list, MedicineItem item) {
							mHelper.addItemToList(item, list);
							updateList();
						}
					});
					adder.editList(mItem);
				}
			});
		}
		return view;
	}

	private class ViewHolder {
		public CheckBox item;
		public TextView itemView;
	}

	public abstract static class OnCheckedChangedListenerLists {
		public abstract void onCheckedChanged(ItemsList list,
				MedicineItem item, boolean isChecked);
	}

}
