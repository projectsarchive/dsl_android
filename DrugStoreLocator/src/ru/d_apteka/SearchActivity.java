package ru.d_apteka;

import java.util.ArrayList;
import java.util.HashMap;

import ru.d_apteka.CustomAutoCompleteTextView.OnShowDropDownListener;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Interpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;

import de.viktorreiser.toolbox.util.AndroidUtils;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup.OnQuickActionListener;
import de.viktorreiser.toolbox.widget.SwipeableListView;

public class SearchActivity extends MyActivity implements OnQuickActionListener {
	/**
	 * Autocomplete text view used to get the query search and show quick
	 * results
	 */
	private CustomAutoCompleteTextView textView;
	/** Button to perform search with the text typed in the {@link #textView} */
	private Button btnSearch;
	/**
	 * List view to present results of search made with the query string from
	 * {@link #textView} after pressing Enter or btnSearch
	 */
	// private ListView listSearch;
	private SwipeableListView listSearch;
	/** Adapter used to fill in the autocomplete {@link #textView} hint-list */
	private QuickSearchAdapter autoCompleteAdapter;
	/**
	 * The adapter used to fill the main list - {@link #listSearch}. It loads
	 * data by portions using {@link ItemDynamicViewAdapter}.
	 */
	private ItemDynamicViewAdapter fullListDynamicAdapter;
	/**
	 * Position of the element clicked in the dropdown list of {@link #textView}
	 * before going to the Availability Activity
	 */
	private int positionSelectedDropDown = -1;
	private boolean showDropDown = true;
	private String mAction;
	private boolean stateRestoring = false;
	private LinearLayout mNothingFound;

	// Quick Action >==========================================================

	private static final class QuickAction {
		public static final int DESCRIPTION = 1;
		public static final int LISTS = 2;
	}

	private HiddenQuickActionSetup mQuickActionSetup;

	// Quick Action <==========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.search);

		mAction = getIntent().getAction();

		autoCompleteAdapter = new QuickSearchAdapter(this,
				R.layout.searchlistitem);
		autoCompleteAdapter.setNotifyOnChange(true);
		textView = (CustomAutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		textView.setOnShowDropDownListener(showDropDownListener);
		textView.setThreshold(1);
		textView.setAdapter(autoCompleteAdapter);
		textView.setOnItemClickListener(itemClickListener);
		textView.addTextChangedListener(textChecker);
		textView.setOnEditorActionListener(autoCompleteEditorActionListener);
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/MyriadPro-Regular.otf");
		textView.setTypeface(tf);
		textView.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(textView, 0);
			}
		}, 100);

		TextView label = (TextView) findViewById(R.id.textView1);
		label.setTypeface(tf);

		btnSearch = (Button) findViewById(R.id.searchBtn);
		btnSearch.setOnClickListener(btnSearchClickListener);

		// listSearch = (ListView) findViewById(R.id.searchResultsList);
		// listSearch = (SwipeableListView)
		// findViewById(R.id.searchResultsList);
		listSearch = (SwipeableListView) findViewById(android.R.id.list);

		SavedSearchState savedState = (SavedSearchState) getLastNonConfigurationInstance();
		ArrayList<MedicineItem> sResultsArr = new ArrayList<MedicineItem>();
		String curTerm = "";
		int page = 0;
		if (savedState != null) {
			sResultsArr = savedState.items;
			curTerm = savedState.searchTerm;
			page = savedState.page;
			textView.dismissDropDown();
			showDropDown = false;
		}
		fullListDynamicAdapter = new ItemDynamicViewAdapter(this,
				R.layout.medicine_item_search, R.id.text1,
				R.layout.progress_item, sResultsArr, curTerm, page);

		// listSearch.setAdapter(fullListDynamicAdapter);
		listSearch.setOnItemClickListener(textViewItemClickListener);

		// Add clear button
		// final Drawable x =
		// getResources().getDrawable(R.drawable.ic_tab_clear);
		final Drawable x = getResources().getDrawable(
				R.drawable.ui_textfield_clear_btn);// your x image, this one
													// from standard android
													// images looks pretty
													// good actually
		x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
		// textView.setCompoundDrawables(null, null, value.equals("") ? null :
		// x, null);
		textView.setCompoundDrawables(null, null, null, null);
		textView.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (textView.getCompoundDrawables()[2] == null) {
					return false;
				}
				if (event.getAction() != MotionEvent.ACTION_UP) {
					return false;
				}
				if (event.getX() > textView.getWidth()
						- textView.getPaddingRight() - x.getIntrinsicWidth()) {
					textView.setText("");
					textView.setCompoundDrawables(null, null, null, null);
					resetSearchResults();
				}
				return false;
			}
		});
		textView.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				textView.setCompoundDrawables(null, null, textView.getText()
						.toString().equals("") ? null : x, null);
				showDropDown = true;
				resetSearchResults();
			}

			public void afterTextChanged(Editable arg0) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});

		// Quick Action >======================================================
		setupQuickAction();

		fullListDynamicAdapter.setQuickActionSetup(mQuickActionSetup);
		listSearch.setAdapter(fullListDynamicAdapter);
		listSearch.setOnScrollListener(new OnScrollListener() {

			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (scrollState == SCROLL_STATE_TOUCH_SCROLL)
					hideSoftKeyboard();

			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		});
		// setListAdapter(fullListDynamicAdapter);
		// Quick Action >======================================================
		mNothingFound = (LinearLayout) findViewById(android.R.id.empty);
		mNothingFound.setVisibility(View.GONE);
		// fullListDynamicAdapter.registerDataSetObserver(new DataSetObserver()
		// {
		// /* (non-Javadoc)
		// * @see android.database.DataSetObserver#onChanged()
		// */
		// @Override
		// public void onChanged() {
		// super.onChanged();
		// if(fullListDynamicAdapter.getCount() == 0) {
		// mNothingFound.setVisibility(View.VISIBLE);
		// }
		// }
		// });
	}

    /** Hides the soft keyboard from the screen */
	private void hideSoftKeyboard() {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private OnShowDropDownListener showDropDownListener = new OnShowDropDownListener() {

		public boolean onShowDropDown(AutoCompleteTextView textView) {
			return !stateRestoring && showDropDown;
		}
	};

	private OnEditorActionListener autoCompleteEditorActionListener = new OnEditorActionListener() {

		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				performSearch();
				return true;
			} else {
				showDropDown = true;
			}
			return false;
		}
	};

	private OnItemClickListener textViewItemClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			try {
				MedicineItem item = (MedicineItem) arg0.getItemAtPosition(arg2);
				showItemInformation(item, mAction);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * Fills autocomplete list if no item is clicked and the search field is not
	 * empty
	 * 
	 * @see FullSearchTask
	 */
	private void performSearch() {
		if (stateRestoring) {
			// hideSoftKeyboard();
			return;
		}
        String term = textView.getText().toString().trim();
		if (term.length() > 0) {
			// Hide keyboard after clicking search
			// see - http://www.gaanza.com/blog/hide-soft-keyboard-android/
			hideSoftKeyboard();

			fullListDynamicAdapter.setSearchTerm(term);
		}
		textView.dismissDropDown();
		showDropDown = false;
        sendGAInfoSearchString(term);
	}

    private void sendGAInfoSearchString(String searchString) {
        EasyTracker.getInstance(this).set(Fields.customDimension(1), searchString);
    }

	/**
	 * Open the activity with some additional information about the item. The
	 * activity is chosen based on the {@link #mAction}. Default is
	 * {@link AvailabilityListActivity}.
	 * 
	 * @param item
	 *            Item detailed information to show about
	 * @param action TODO
	 */
	private void showItemInformation(MedicineItem item, String action) {
		if (item == null)
			return;

		Intent intent = new Intent();
		// This is redundant because the workflow was changed. The action is now
		// always "ACTION_AVAILABILITY"
		if (action.equals("ACTION_DETAILS")) {
			intent.setClass(this, DetailsActivity.class);
			intent.putExtra("showAvailabilityBtn", true);
		} else {
			intent.setClass(this, AvailabilityListActivity.class);
			intent.putExtra("showDetailsBtn", true);
		}
		intent.putExtra("item", item);
		startActivity(intent);
	}

	private View.OnClickListener btnSearchClickListener = new View.OnClickListener() {

		public void onClick(View v) {
			performSearch();
		}

	};

	private final OnItemClickListener itemClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			try {
				MedicineItem item = (MedicineItem) arg0.getItemAtPosition(arg2);
				if (item != null) {
					positionSelectedDropDown = arg2;
					showItemInformation(item, mAction);
					performSearch();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * Stops the search task running and clears all the previously generated
	 * results
	 */
	private void resetSearchResults() {
		if (stateRestoring)
			return;
		fullListDynamicAdapter.resetAdapter();
	}

	private final TextWatcher textChecker = new TextWatcher() {

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			resetSearchResults();
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void afterTextChanged(Editable s) {
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		if (positionSelectedDropDown != -1)
			listSearch.setSelection(positionSelectedDropDown);
		positionSelectedDropDown = -1;
		fullListDynamicAdapter.getData();
		super.onResume();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		fullListDynamicAdapter.cancelCurrentTask();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onRetainNonConfigurationInstance()
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		SavedSearchState state = new SavedSearchState();
		state.items = fullListDynamicAdapter.getItems();
		state.searchTerm = fullListDynamicAdapter.getSearchTerm();
		state.page = fullListDynamicAdapter.getCurPage();
		return state;
	}

	public class SavedSearchState {
		public ArrayList<MedicineItem> items;
		public String searchTerm;
		public int page;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		stateRestoring = true;
		super.onRestoreInstanceState(savedInstanceState);
		stateRestoring = false;
		showDropDown = false;
	}

	// Quick Action >==========================================================

	/**
	 * React on quick action click.
	 */
	public void onQuickAction(AdapterView<?> parent, View view, int position,
			int quickActionId) {
		MedicineItem item = fullListDynamicAdapter.getItem(position);
		switch (quickActionId) {
			case QuickAction.DESCRIPTION:
//				Intent intent = new Intent(mActivity, DetailsActivity.class);
//				intent.putExtra("showAvailabilityBtn", true);
//				intent.putExtra("item", item);
//				startActivity(intent);
				showItemInformation(item, "ACTION_DETAILS");
				break;

			case QuickAction.LISTS:
				ListsManager manager = new ListsManager(mActivity);
				manager.manageListsForItem(item);
				break;
		}
	}

	/**
	 * Create a global quick action setup.
	 */
	private void setupQuickAction() {
		mQuickActionSetup = new HiddenQuickActionSetup(this);
		mQuickActionSetup.setOnQuickActionListener(this);

		// a nice cubic ease animation
		mQuickActionSetup.setOpenAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				v -= 1;
				return v * v * v + 1;
			}
		});
		mQuickActionSetup.setCloseAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				return v * v * v;
			}
		});

		int imageSize = AndroidUtils.dipToPixel(this, 40);

		mQuickActionSetup.setBackgroundResource(R.drawable.ui_qa_background);
		mQuickActionSetup.setImageSize(imageSize, imageSize);
		mQuickActionSetup.setAnimationSpeed(700);
		mQuickActionSetup.setStartOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setStopOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setSwipeOnLongClick(true);

		mQuickActionSetup.addAction(QuickAction.DESCRIPTION,
				R.string.search_activity_qa_description,
				R.drawable.ui_qa_description);
		mQuickActionSetup.addAction(QuickAction.LISTS,
				R.string.search_activity_qa_lists, R.drawable.ui_qa_lists);
	}

	// Quick Action <==========================================================
}
