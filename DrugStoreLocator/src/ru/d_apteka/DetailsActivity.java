/**
 * 
 */
package ru.d_apteka;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Activity class for representing the details of a given {@link MedicineItem}
 * 
 * @author stas
 * 
 */
public class DetailsActivity extends ItemRelatedActivity {
	private GetDetailsTask getDetailsTask;
	private WebView mWebView;
	private Handler handler = null;
	private boolean pageLoaded = false;
	private boolean dataRecieved = false;
	private DetailsViewerClient detailsViewerClient;
	private String mQueryResult = null;

	private OnTaskCompleteListener<String> mTaskCompleteListener = new OnTaskCompleteListener<String>() {

		public void onTaskComplete(String result) {
			dataRecieved = true;
			if (pageLoaded) {
				// if (progressDialog != null && progressDialog.isShowing())
				// progressDialog.dismiss();
				mQueryResult = null;
				showItemDescriptionInWeb(result);
			} else {
				mQueryResult = result;
			}
		}
	};
	private ProgressDialog progressDialog;

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.item_details);

		getDetailsTask = new GetDetailsTask((MyApp) getApplication());

		this.setTitle(mItem.getName());

		if (mExtras.getBoolean("showAvailabilityBtn") == true) {
		}

		handler = new Handler();

		getDetailsTask.setOnTaskCompleteListener(mTaskCompleteListener);

		getDetailsTask.execute(mItem);

		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage(getString(R.string.loading_common));
		progressDialog.show();

		initializeDetailsWebView();
		detailsViewerClient.setListener(new PageFinishedListener() {

			public void onPageFinished() {
				pageLoaded = true;
				if (dataRecieved) {
					if (mQueryResult != null) {
						showItemDescriptionInWeb(mQueryResult);
						mQueryResult = null;
					}
					if (progressDialog != null && progressDialog.isShowing())
						progressDialog.dismiss();
				}
			}
		});
		
	}

    /* (non-Javadoc)
         * @see ru.d_apteka.ItemRelatedActivity#_onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
         */
	@Override
	protected void _onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super._onCreateContextMenu(menu, v, menuInfo);
		if (mExtras.getBoolean("showAvailabilityBtn") == true)
			menu.add(Menu.NONE, 2, 2, getString(R.string.show_availability));
	}
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.ItemRelatedActivity#_onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	protected boolean _onContextItemSelected(MenuItem item) {
		if(item.getItemId() == 2) {
			Intent intent = new Intent(mActivity, AvailabilityListActivity.class);
			intent.putExtra("item", mItem);
			startActivity(intent);
			return true;
		} else {
			return false;
		}
	}

	private void showItemDescriptionInWeb(String json) {
		String functionCall = "javascript:fillData(" + json + ")";
		loadURL(functionCall);
	}

	private void initializeDetailsWebView() {
		mWebView = (WebView) findViewById(R.id.itemDescriptionWeb);
		mWebView.setVisibility(View.GONE);
		mWebView.setBackgroundColor(0);
		mWebView.setPadding(0, 0, 0, 0);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setDefaultTextEncodingName("utf-8");

		// Load the URLs inside the WebView, not in the external web browser
		detailsViewerClient = new DetailsActivity.DetailsViewerClient();
		mWebView.setWebViewClient(detailsViewerClient);
		mWebView.setWebChromeClient(new DetailsChromeClient());
		mWebView.addJavascriptInterface(new AndroidInteraction(),
				"AndroidInteraction");
		mWebView.addJavascriptInterface(new JSCommon(), "JSCommon");

		// In case of problems with Access-Control-Allow-Origin at Android 4.1
		// http://stackoverflow.com/questions/11318703/access-control-allow-origin-error-at-android-4-1
		// if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN)
		// mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

		// Load a page
		Handler handler = new Handler();
		handler.post(new Runnable() {
			public void run() {
				mWebView.loadUrl("file:///android_asset/www/index.html");
			}
		});

	}

	public void loadPage(String in) {
		final String url = "file:///android_asset/www/" + in;
		loadURL(url);
	}

	private void loadURL(final String in) {
		handler.post(new Runnable() {
			public void run() {
				mWebView.loadUrl(in);
			}
		});
	}

	public static class DetailsViewerClient extends WebViewClient {
		public boolean isFinished = false;
		private PageFinishedListener listener;

		/**
		 * {@inheritDoc}
		 * 
		 * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView,
		 *      java.lang.String)
		 */
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl("javascript:changeLocation('" + url + "')");
			return true;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see android.webkit.WebViewClient#onPageFinished(android.webkit.WebView,
		 *      java.lang.String)
		 */
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			isFinished = true;
			if (listener != null) {
				listener.onPageFinished();
				listener = null;
			}
		}

		/**
		 * @param listener
		 *            the listener to set
		 */
		public void setListener(PageFinishedListener listener) {
			this.listener = listener;
		}

	}

	private interface PageFinishedListener {
		public void onPageFinished();
	}

	protected class AndroidInteraction {
		public void onDataRendered() {
			// http://stackoverflow.com/questions/9446868/access-ui-from-javascript-on-android
			handler.post(new Runnable() {

				public void run() {
					if (progressDialog != null && progressDialog.isShowing())
						progressDialog.dismiss();
					// Log.i("TAG", Thread.currentThread().getName());
					mWebView.setVisibility(View.VISIBLE);
				}
			});
		}
	}

}
