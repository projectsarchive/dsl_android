package ru.d_apteka;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

/**
 * <p>Extends {@link ArrayAdapter}<{@link MedicineItem}>, this class performs querying the server
 * for items in the <code>performFiltering</code> method. Since the method executes in another thread,
 * this is the best solution to query the server.</p>
 * @author stas
 *
 */
public class QuickSearchAdapter extends ArrayAdapter<MedicineItem> {

	private MyFilter mFilter = new MyFilter();
	private ArrayList<MedicineItem> mData;
	private Typeface mTypeface;
	/** Number of items to display for the autocomplete */
	private static final Integer autoCompleteRows = 10;

    public QuickSearchAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		mData = new ArrayList<MedicineItem>();
		mTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/MyriadPro-Regular.otf");
	}
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	View v = super.getView(position, convertView, parent);
    	if(TextView.class.isInstance(v)) {
    		TextView tv = (TextView) v;
    		tv.setTypeface(mTypeface);
    		return tv;
    	}
    	return v;
    }
	
    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public MedicineItem getItem(int index) {
    	if(mData.size() < index)
    		return null;
        return mData.get(index);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
    
    protected class MyFilter extends Filter {
    	
    	@Override
        protected FilterResults performFiltering(CharSequence constraint) {
    		//showResults = true;
            FilterResults filterResults = new FilterResults();
            if(constraint != null) {
                try {
                    String term = constraint.toString().trim();
                    mData = MedicineItemFetcher.findByName(term, autoCompleteRows);
                }
                catch(Exception e) {
                	mData.clear();
                }
                // Now assign the values and count to the FilterResults object
                filterResults.values = mData;
                filterResults.count = mData.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
        	if(results != null && results.count > 0) {
            notifyDataSetChanged();
            }
            else {
                notifyDataSetInvalidated();
            }
        }
    }
	
}
