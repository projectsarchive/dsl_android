/**
 *
 */
package ru.d_apteka;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/**
 * Web resources reader class
 *
 * @author stas
 */
public class WebReader implements IWebReader {

    HttpClient client;

    public WebReader() {
        client = new DefaultHttpClient();
    }

    public WebReader(HttpClient httpClient) {
        client = httpClient;
    }

    /**
     * @see #readWebResource(String, ArrayList, Boolean)
     */
    public String readWebResource(String path) throws IOException {
        return readWebResource(path, true);
    }

    /**
     * @see #readWebResource(String, ArrayList, Boolean)
     */
    public String readWebResource(String path, Boolean compression) throws IOException {
        ArrayList<BasicNameValuePair> arr = new ArrayList<BasicNameValuePair>();
        return readWebResource(path, arr, compression);
    }

    /**
     * @see #readWebResource(String, ArrayList, Boolean)
     */
    public String readWebResource(String path, ArrayList<BasicNameValuePair> params) throws IOException {
        return readWebResource(path, params, true);
    }

    /**
     * Reads the web resource at the specified path with the params given.
     *
     * @param path        Path of the resource to be read.
     * @param params      Parameters needed to be transferred to the server using POST method.
     * @param compression If it's needed to use compression. Default is <b>true</b>.
     * @return <p>Returns the string got from the server. If there was an error downloading file,
     * an empty string is returned, the information about the error is written to the log file.</p>
     * @throws IOException When it's impossible to get the result
     */
    public String readWebResource(String path, ArrayList<BasicNameValuePair> params, Boolean compression) throws IOException {
        String result = "";

        if (params.size() > 0) {
            path += "?" + URLEncodedUtils.format(params, "utf-8");
        }
        HttpGet httpGet = new HttpGet(path);

        if (compression)
            httpGet.addHeader("Accept-Encoding", "gzip");

        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                if (entity.getContentEncoding() != null
                        && "gzip".equalsIgnoreCase(entity.getContentEncoding()
                        .getValue()))
                    result = uncompressInputStream(content);
                else
                    result = convertStreamToString(content);
            } else {
                Log.e(MyApp.class.toString(), "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        return result;
    }

    private String uncompressInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder value = new StringBuilder();

        GZIPInputStream gzipIn = null;
        InputStreamReader inputReader = null;
        BufferedReader reader = null;

        try {
            gzipIn = new GZIPInputStream(inputStream);
            inputReader = new InputStreamReader(gzipIn, "UTF-8");
            reader = new BufferedReader(inputReader);

            String line = "";
            while ((line = reader.readLine()) != null) {
                value.append(line + "\n");
            }
        } finally {
            try {
                if (gzipIn != null) {
                    gzipIn.close();
                }

                if (inputReader != null) {
                    inputReader.close();
                }

                if (reader != null) {
                    reader.close();
                }

            } catch (IOException io) {

                io.printStackTrace();
            }

        }

        return value.toString();
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
