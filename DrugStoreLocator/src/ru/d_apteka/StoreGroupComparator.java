package ru.d_apteka;

import java.util.Comparator;

public class StoreGroupComparator implements Comparator<Store>{

	public int compare(Store lhs, Store rhs) {
		return lhs.getGroupId().compareToIgnoreCase(rhs.getGroupId());
	}
}