/**
 * 
 */
package ru.d_apteka;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

class GetAvailabilityTask extends
		ApiGetterTask<DynamicTaskParams<MedicineItem>, ArrayList<ItemInStore>> {
	
	private SmartLocationTeller mSmartLocationListener;

	/**
	 * @param app
	 */
	public GetAvailabilityTask(MyApp app, SmartLocationTeller smartLocationListener) {
		super(app);
		mSmartLocationListener = smartLocationListener;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.ApiGetterTask#getPath()
	 */
	@Override
	protected String getPath() {
		return MyApp.server + "/items/:itemid/availability";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ArrayList<ItemInStore> processOutput(String result,
			DynamicTaskParams<MedicineItem>... params) {
		MedicineItem item = params[0].getParams();
		ArrayList<ItemInStore> resArray = new ArrayList<ItemInStore>();

		try {
			JSONObject res = new JSONObject(result);
            Location currentLocation = mSmartLocationListener.getCurrentLocation();
            Location storeLocation = new Location("");
            Double distance;
			JSONArray jArray = res.getJSONArray("response");
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject json_data = jArray.getJSONObject(i);

                JSONObject store_info = json_data.getJSONObject("store");

				Store store = app.getStoreById(store_info.getString("sid"));
				if (store == null)
					continue;
				ItemInStore iis = new ItemInStore(store, item,
						json_data.getDouble("quantity"), json_data.getDouble("price"));

                if(currentLocation != null && mSmartLocationListener.isLocationAccurate()) {
                    storeLocation.setLatitude(store_info.getDouble("lat"));
                    storeLocation.setLongitude(store_info.getDouble("lon"));
                    distance = (double) currentLocation.distanceTo(storeLocation);
                    iis.setDistance(distance / 1000);
                }

				resArray.add(iis);
			}
		} catch (JSONException e) {
			Log.e(this.toString(), "Error parsing data " + e.toString());
			return null;
		}
		return resArray;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ArrayList<BasicNameValuePair> getUriParams(
			DynamicTaskParams<MedicineItem>... params) {
		ArrayList<BasicNameValuePair> resultParams = new ArrayList<BasicNameValuePair>();
		MedicineItem item = params[0].getParams();
		resultParams.add(new BasicNameValuePair("page", Integer
				.toString(params[0].getPage() + 1)));
		resultParams.add(new BasicNameValuePair("per_page", Integer
				.toString(params[0].getPageSize())));
		resultParams.add(new BasicNameValuePair("itemid", item.getId()
				.toString()));
		resultParams.add(new BasicNameValuePair("cityid", MyApp
				.getCurrentCity(app.getApplicationContext()).toString()));

        LatLng p = mSmartLocationListener.getCurrentPosition();
        if (p != null && mSmartLocationListener.isLocationAccurate()) {
            resultParams.add(new BasicNameValuePair("lon", Double.toString(p.longitude)));
            resultParams.add(new BasicNameValuePair("lat", Double.toString(p.latitude)));
        }

		return resultParams;
	}

    @Override
    protected int getInternetErrorMessage() {
        return R.string.internet_error_availability;
    }
}