package ru.d_apteka;

public class City {
	private String mName;
	private String mId;
	
	public City(String name) {
		super();
		this.mName = name;
	}

	public String getName() {
		return mName;
	}
	
	public void setName(String name) {
		this.mName = name;
	}

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}
	
	
}
