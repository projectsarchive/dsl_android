package ru.d_apteka;

import java.util.*;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import ru.d_apteka.SmartLocationTeller.SmartLocationListener;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MapActivity extends FragmentActivity implements OnTaskCompleteListener<Void>, OnMarkerStoresReadyListener, GoogleMap.OnInfoWindowClickListener{
	
	private GoogleMap map;
    private ArrayList<Store> stores;
	private ProgressDialog waitDialog;
	private MyApp app;
	private SmartLocationTeller mSmartLocationListener;
	private String mStoreId;
    private HashMap<Marker,Store> markerStores = new HashMap<Marker, Store>();

    @Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
        int available = ConnectionResult.SERVICE_MISSING;
        try {
            available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        } catch(Exception e) {}
        if(available != ConnectionResult.SUCCESS) {
            setContentView(R.layout.map_2_no_google_play);
            Button goToServices = (Button) findViewById(R.id.open_services_link);
            goToServices.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=com.google.android.gms"));
                    startActivity(intent);
                }
            });
            return;
        }

		setContentView(R.layout.map_2);

		TextView mTitleTextView = (TextView) findViewById(R.id.titleText);
		mTitleTextView.setText(getTitle());
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/MyriadPro-Regular.otf");
		mTitleTextView.setTypeface(tf);
		
		mSmartLocationListener = new SmartLocationTeller(this);
		mSmartLocationListener.setNeedGPS(true);
		mSmartLocationListener.setListener(new SmartLocationListener() {
			
			public void onLocationChanged(Location newLocation) {
				if(newLocation == null)
					return;
				// TODO Move the marker not the map
//				mc.animateTo(SmartLocationTeller.getGeopoint(newLocation));
			}
		});
		app = (MyApp)getApplicationContext();
		
		waitDialog = new ProgressDialog(this);
    	// Show current message in progress dialog
    	waitDialog.setMessage(getString(R.string.loadingStoresDialog));
		
		map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMap();

        AlvikInfoWindowAdapter infoWindowAdapter = new AlvikInfoWindowAdapter(getLayoutInflater());
        map.setInfoWindowAdapter(infoWindowAdapter);
        map.setOnInfoWindowClickListener(this);

        LatLng p = mSmartLocationListener.getCurrentPosition();
        if(p == null) {
        	Toast.makeText(this, "Current location is unavailable", Toast.LENGTH_SHORT);
        } else {
        	map.moveCamera(CameraUpdateFactory.newLatLng(p));
        }

        float zoomLevel = 15.5f;
        if(!mSmartLocationListener.isLocationAccurate()) zoomLevel = 13f;
        map.moveCamera(CameraUpdateFactory.zoomTo(zoomLevel));
        
        Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if(extras != null)
			mStoreId = extras.getString("storeId");
        
        stores = app.getStoresList(mStoreId);
        
        ReadStoresTask task = app.getReadStoresTask();
		if(task.getStatus() != AsyncTask.Status.FINISHED){
        	task.setOnTaskFinishListener(this);
        if (!waitDialog.isShowing()) {
        		waitDialog.show();
        	}
        }else
        	PositionStoresOnMap();
        
        positionMapIntent();
	}
	
	private int getIconForGroup(String groupId){
    	int iconId = getResources().getIdentifier("drawable/trademark" + groupId, "drawable", getPackageName());
    	if(iconId != 0)
    		return iconId;
    	else
    		return R.drawable.otherstores;
    }
    
    @SuppressWarnings("unchecked")
	private void PositionStoresOnMap(){
    	new PositionStoresOnMapTask().addListener(this).execute(stores);
    }

    public void onTaskComplete(Void result) {
		PositionStoresOnMap();
		if(waitDialog != null && waitDialog.isShowing()){
			waitDialog.dismiss();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	/**
	 * {@inheritDoc}
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
        if(mSmartLocationListener != null) {
		    mSmartLocationListener.onStop();
        }
        EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		if(mSmartLocationListener != null) {
            mSmartLocationListener.onStart();
        }
        EasyTracker.getInstance(this).activityStart(this);
	}

    public void onMarkerStoresReady(HashMap<Marker, Store> markerStores) {
        this.markerStores = markerStores;
    }

    public void onInfoWindowClick(Marker marker) {
        Store store = markerStores.get(marker);
        if(store != null) {
            Context context = this;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName(context, StoreDetailsActivity.class.getName());
            intent.putExtra("title", marker.getTitle());
            intent.putExtra("details", marker.getSnippet());
            intent.putExtra("storeSid", store.getId());
            context.startActivity(intent);
        }
    }

    private class PositionStoresOnMapTask extends AsyncTask<ArrayList<Store>, Void, Void>{
		private HashMap<MarkerOptions, Store> markers = new HashMap<MarkerOptions, Store>();
        private HashMap<Marker, Store> markerStores = new HashMap<Marker, Store>();
        private ArrayList<OnMarkerStoresReadyListener> listeners = new ArrayList<OnMarkerStoresReadyListener>();

        public PositionStoresOnMapTask addListener(OnMarkerStoresReadyListener listener) {
            listeners.add(listener);
            return this;
        }
		
		@Override
		protected Void doInBackground(ArrayList<Store>... params) {
	    	ArrayList<Store> locStores = params[0]; 
			Collections.sort(locStores, new StoreGroupComparator());
	    	String lastGroup = "";
            int iconRes = R.drawable.otherstores;
	    	for (Store store : locStores) {
	    		if(!lastGroup.equals(store.getGroupId())){
	    			lastGroup = store.getGroupId();
	    			iconRes = getIconForGroup(lastGroup);
	    		}
                markers.put(new MarkerOptions()
                        .position(store.getPoint())
                        .title(store.getName())
                        .snippet(store.getAddress())
                        .icon(BitmapDescriptorFactory.fromResource(iconRes)),
                        store);
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

            for(Map.Entry<MarkerOptions, Store> entry : markers.entrySet()) {
                Marker marker = map.addMarker(entry.getKey());
                markerStores.put(marker, entry.getValue());
            }

            for(OnMarkerStoresReadyListener listener : listeners) {
                listener.onMarkerStoresReady(markerStores);
            }
		}
	}
	
	@Override
	public void onNewIntent(Intent newIntent) {
		if(newIntent.getExtras() == null) {
			return;
		}
		mStoreId = newIntent.getExtras().getString("storeId");
		positionMapIntent();
		super.onNewIntent(newIntent);
	}
	

	/**
	 * Positions the map on the store passed through intent via Extras
     */
	private void positionMapIntent() {
		Store store = app.getStoreById(mStoreId);
		if(store == null)
			return;
		map.moveCamera(CameraUpdateFactory
                .newLatLng(store.getPoint()));
	}
}

interface OnMarkerStoresReadyListener {
    void onMarkerStoresReady(HashMap<Marker, Store> markerStores);
}
