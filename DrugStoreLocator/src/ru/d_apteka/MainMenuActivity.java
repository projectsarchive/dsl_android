/**
 * 
 */
package ru.d_apteka;

import java.io.IOException;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.zxing.integration.android.IntentIntegrator;

/**
 * @author stas
 * 
 */
public class MainMenuActivity extends MyActivity {
	private CodeSearchTask task;
	private ProgressDialog waitDialog;
	private Context context;
	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);

        GoogleAnalytics.getInstance(this).getTracker(getString(R.string.ga_trackingId));

		// >>> MyActivity code since we inherit this class from MapActivity, not
		// Activity
//		TextView mTitleTextView = (TextView) findViewById(R.id.titleText);
//		mTitleTextView.setText(getTitle());
//		Button homeButton = (Button) findViewById(R.id.titleGoToMain);
//		homeButton.setVisibility(View.GONE);
		// <<< MyActivity code

		this.context = getApplicationContext();

		waitDialog = new ProgressDialog(this);
		waitDialog.setMessage(getString(R.string.searching_code));

		GridView grid = (GridView) findViewById(R.id.grid);
		grid.setAdapter(new MainMenuAdapter(this));
		grid.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				MainMenuItem item = (MainMenuItem) parent
						.getItemAtPosition(position);
				executeItem(item, position);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 *      android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// requestCode is the index of the "Scanner" option in the main menu
		if (requestCode == IntentIntegrator.REQUEST_CODE) {
			// Scanner results
			if (resultCode == RESULT_OK) {
				String contents = data.getStringExtra("SCAN_RESULT");
				// String format = data.getStringExtra("SCAN_RESULT_FORMAT");
				// Handle successful scan
				// processScanCode(contents, format);
				task = new CodeSearchTask();
				task.execute(contents);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
				// Do nothing
			}
		}
	}

	private void executeItem(MainMenuItem item, int position) {
		if (item.intent != null) {
            if("ru.d_apteka.SCAN".equals(item.intent.getAction())) {
                IntentIntegrator.initiateScan(
                        this,
                        IntentIntegrator.PRODUCT_CODE_TYPES,
                        getString(R.string.barcode_scanner_explain)
                );
            } else {
			    startActivityForResult(item.intent, position);
            }
        } else {
			item.activity = this;
			item.function.onCall(item);
		}
	}

	private void processScanCode(ArrayList<MedicineItem> items) {
		int count = items == null ? 0 : items.size();
		if (count == 0) {
			Toast toast = Toast.makeText(this, R.string.no_items_code,
					Toast.LENGTH_SHORT);
			toast.show();
		} else if (count == 1) {
			MedicineItem item = items.get(0);
			Intent intent = new Intent(this, AvailabilityListActivity.class);
			intent.putExtra("item", item);
			intent.putExtra("showDetailsBtn", true);
			startActivity(intent);
		} else if (count > 1) {
			Intent intent = new Intent(this, ScanCodeActivity.class);
			intent.putExtra("items", items);
			startActivity(intent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private class CodeSearchTask extends
			AsyncTask<String, Void, ArrayList<MedicineItem>> {
		private boolean internetError = false;

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected ArrayList<MedicineItem> doInBackground(String... params) {
			// Suppose there are not so many items with the same code, so don't
			// bother with the pagination
			try {
				return MedicineItemFetcher.findBy1DCode(params[0]);
			} catch (IOException e) {
				internetError = true;
				return null;
			}
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			waitDialog.show();
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see android.os.AsyncTask#onCancelled()
		 */
		@Override
		protected void onCancelled() {
			super.onCancelled();
			waitDialog.dismiss();
			onFinish();
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(ArrayList<MedicineItem> result) {
			if (waitDialog != null && waitDialog.isShowing())
				waitDialog.dismiss();
			if (!this.internetError)
				processScanCode(result);
			onFinish();
		}

		protected void onFinish() {
			if (this.internetError) {
				try {
					Toast toast = Toast.makeText(context,
							R.string.internet_error_scan,
							Toast.LENGTH_SHORT);
					toast.show();
				} catch (Exception e) {
				}
			}
		}
	}
}
