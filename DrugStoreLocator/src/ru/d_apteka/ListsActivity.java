package ru.d_apteka;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import de.viktorreiser.toolbox.util.AndroidUtils;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup.OnQuickActionListener;


public class ListsActivity extends MyListActivity implements OnQuickActionListener {
	private ListsAdapter mListsAdapter;
	private UsersDatabaseHelper mHelper;
	private ListView mListView;

	// Quick Action <==========================================================
	
	private static final class QuickAction {
		public static final int DESCRIPTION = 1;
		public static final int DELETE = 2;
	}

	private HiddenQuickActionSetup mQuickActionSetup;
	private ItemsList mList;
	
	// Quick Action >==========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.lists_activity);
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras();
		if(!extras.containsKey("listId")) {
            Log.e("ListsActivity", "listId is not specified.");
            return;
		}
		long listId = extras.getLong("listId");
		mHelper = new UsersDatabaseHelper(mActivity);
		
		mList = mHelper.getListById(listId); 
		mListsAdapter = new ListsAdapter(mActivity, mList);
		setTitle(mList.getName());
		
//		mListView = (ListView) findViewById(R.id.lists_list_view);
		mListView = (ListView) findViewById(android.R.id.list);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MedicineItem item = (MedicineItem) arg0.getItemAtPosition(arg2); 
				Intent intent = new Intent(mActivity, AvailabilityListActivity.class);
				intent.putExtra("item", item);
				intent.putExtra("showDetailsBtn", true);
				startActivity(intent);
			}
		});
		
		// Quick Action <======================================================
		setupQuickAction();

		mListsAdapter.setQuickActionSetup(mQuickActionSetup);
//		mListView.setAdapter(mListsAdapter);
		setListAdapter(mListsAdapter);
		// Quick Action >======================================================
	}
	
	// Quick Action <==========================================================
	
	/**
	 * React on quick action click.
	 */
	public void onQuickAction(AdapterView<?> parent, View view, int position, int quickActionId) {
		MedicineItem item = mListsAdapter.getItem(position);
		switch (quickActionId) {
		case QuickAction.DESCRIPTION:
			Intent intent = new Intent(mActivity, DetailsActivity.class);
			intent.putExtra("showAvailabilityBtn", true);
			intent.putExtra("item", item);
			startActivity(intent);
			break;

		case QuickAction.DELETE:
			mHelper.removeItemFromList(item, mList);
			mListsAdapter.notifyDataSetChanged();
			break;
		}
	}

	/**
	 * Create a global quick action setup.
	 */
	private void setupQuickAction() {
		mQuickActionSetup = new HiddenQuickActionSetup(this);
		mQuickActionSetup.setOnQuickActionListener(this);

		// a nice cubic ease animation
		mQuickActionSetup.setOpenAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				v -= 1;
				return v * v * v + 1;
			}
		});
		mQuickActionSetup.setCloseAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				return v * v * v;
			}
		});

		int imageSize = AndroidUtils.dipToPixel(this, 40);

		mQuickActionSetup.setBackgroundResource(R.drawable.ui_qa_background);
		mQuickActionSetup.setImageSize(imageSize, imageSize);
		mQuickActionSetup.setAnimationSpeed(700);
		mQuickActionSetup.setStartOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setStopOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setSwipeOnLongClick(true);

		mQuickActionSetup.addAction(QuickAction.DESCRIPTION,
				R.string.search_activity_qa_description, R.drawable.ui_qa_description);
		mQuickActionSetup.addAction(QuickAction.DELETE,
				R.string.search_activity_qa_delete_item, R.drawable.ui_qa_delete_item);
	}

	// Quick Action >==========================================================
}
