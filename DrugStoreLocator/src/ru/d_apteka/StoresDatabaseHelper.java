package ru.d_apteka;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class StoresDatabaseHelper {
	public static final String DATABASE_NAME = "stores.db";
	public static final int DATABASE_VERSION = 3;
	public static final String TABLE_NAME = "stores";
//	public static final int emptyLinesNumber = 3000;
	public static final String emptyLinesName = "NoN";
	
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_LONGITUDE = "lon";
	public static final String KEY_LATITUDE = "lat";
	public static final String KEY_STOREID = "storeId";
	public static final String KEY_GROUPID = "groupId";
	public static final String KEY_CITY = "city";
    public static final String KEY_UPDATED = "updated_at";
	
	public static final String VERSION_TABLE_NAME = "data_versions";
	public static final String KEY_VERSION = "data";

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
	
	private Context context;
	private SQLiteDatabase db;
	private OpenHelper openHelper;
	
	public StoresDatabaseHelper(Context context) {
		this.context = context;
		openHelper = new OpenHelper(this.context);
		// Making a new StoresDatabaseHelper object is preferable to create inside non-UI threads
		this.db = openHelper.getWritableDatabase();
	}
	
	public ArrayList<Store> selectAll() {
		ArrayList<Store> list = new ArrayList<Store>();
		Cursor cursor = this.db.query(TABLE_NAME, getStoreFields(),
				KEY_NAME + " <> ?", new String[]{emptyLinesName}, null, null, "groupId desc");
		if (cursor.moveToFirst()) {
			do {
				Store store = createStoreFromCursor(cursor);
				list.add(store);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}

	private String[] getStoreFields() {
		return new String[] { KEY_NAME,
				KEY_ADDRESS, KEY_LATITUDE, KEY_LONGITUDE, KEY_STOREID, KEY_GROUPID, KEY_CITY };
	}

	private Store createStoreFromCursor(Cursor cursor) {
		Store store = new Store(cursor.getDouble(2),
				cursor.getDouble(3), cursor.getString(0),
				cursor.getString(1), cursor.getInt(6));
		store.setGroupId(cursor.getString(5));
		store.setId(cursor.getString(4));
		return store;
	}
	
	private ContentValues getStoreValues(Store store)
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_NAME, store.getName());
		initialValues.put(KEY_ADDRESS, store.getAddress());
		initialValues.put(KEY_LONGITUDE, store.getPoint().longitude);
		initialValues.put(KEY_LATITUDE, store.getPoint().latitude);
		initialValues.put(KEY_STOREID, store.getId());
		initialValues.put(KEY_GROUPID, store.getGroupId());
		initialValues.put(KEY_CITY, store.getCity());
		return initialValues;
	}
	
	public long updateInsertStore(Store store){
		Cursor mCursor = this.db.query(TABLE_NAME, new String[]{KEY_ROWID}, KEY_STOREID + "=?", new String[]{store.getId()}, null, null, null);
		long t = 0;
		if(mCursor != null && mCursor.getCount() > 0){
			mCursor.moveToFirst();
			ContentValues initialValues = getStoreValues(store);
            initialValues.put(KEY_UPDATED, dateFormat.format(new Date()));
			t = db.update(TABLE_NAME, initialValues , KEY_ROWID + "=?" , new String[]{mCursor.getString(0)});
		} else {
			t = insertStore(store);
		}
		if (mCursor != null && !mCursor.isClosed()) {
			mCursor.close();
		}
		return t;
	}
	
	public long insertStore(Store store){
		Cursor mCursor = this.db.query(TABLE_NAME, new String[]{KEY_ROWID}, KEY_NAME + "=?", new String[]{emptyLinesName}, null, null, null);
		
		ContentValues initialValues = getStoreValues(store);
        initialValues.put(KEY_UPDATED, dateFormat.format(new Date()));
		long t = 0;
		if(mCursor != null && mCursor.getCount() > 0){
			mCursor.moveToFirst();
			t = db.update(TABLE_NAME, initialValues, KEY_ROWID + "=?", new String[]{mCursor.getString(0)});
			
		} else {
			t = db.insert(TABLE_NAME, null, initialValues);
		}
		if (mCursor != null && !mCursor.isClosed()) {
			mCursor.close();
		}
		return t;
	}
		
	public int deleteStore(String storeId){
		int res = db.delete(TABLE_NAME, KEY_STOREID + "=?", new String[]{storeId});
		ContentValues initialValues = getEmptyValues();
		//return db.update(TABLE_NAME, initialValues, KEY_STOREID + "=?", new String[]{storeId});
		db.insert(TABLE_NAME, null, initialValues);
		return res;
	}
	
	private ContentValues getEmptyValues() {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_NAME, emptyLinesName);
		initialValues.put(KEY_ADDRESS, Double.toString(1E100));
		initialValues.put(KEY_LONGITUDE, 1E-6);
		initialValues.put(KEY_LATITUDE, 1E-6);
		initialValues.put(KEY_STOREID, Double.toString(1E20));
		initialValues.put(KEY_GROUPID, Double.toString(1E20));
		initialValues.put(KEY_CITY, 10);
		return initialValues;
	}

	public Date loadDate(Cursor cursor, int index) {
        if (cursor.isNull(index)) {
            return null;
        }
        try {
            return dateFormat.parse(cursor.getString(index));
        } catch (ParseException e) {
            Log.e(getClass().toString(), e.toString());
            return new Date(0);
        }
    }


    public long setLastUpdate(Integer cityId, Date date){
        Cursor mCursor = this.db.query(VERSION_TABLE_NAME, new String[]{KEY_ROWID}, KEY_CITY + "=?", new String[]{cityId.toString()}, null, null, null);

        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_CITY, cityId);
        initialValues.put(KEY_VERSION, dateFormat.format(date));
        long t = 0;
        if(mCursor != null && mCursor.getCount() > 0){
            mCursor.moveToFirst();
            t = db.update(VERSION_TABLE_NAME, initialValues, KEY_ROWID + "=?", new String[]{mCursor.getString(0)});

        } else {
            t = db.insert(VERSION_TABLE_NAME, null, initialValues);
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        return t;
    }

    public Date getLastUpdate(Integer cityId) {
//        Cursor cursor = db.query(TABLE_NAME, new String[] {"MAX(" + KEY_UPDATED + ")"}, KEY_CITY + "=?", new String[]{cityId.toString()}, null, null, null);
        Cursor cursor = db.query(VERSION_TABLE_NAME, new String[]{KEY_VERSION}, KEY_CITY + "=?", new String[]{cityId.toString()}, null, null, null);
        Date res = null;
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            res = loadDate(cursor, 0);
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return res;
    }

	/**
	 * Drops tables (main and version) of the database
	 */
	public void dropTables() {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + VERSION_TABLE_NAME);
		openHelper.onCreate(db);
	}
	
	public void close() {
		openHelper.close();
	}
	
	private class OpenHelper extends SQLiteOpenHelper {

		public OpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"" + KEY_NAME + " TEXT NOT NULL, " + KEY_ADDRESS + " TEXT, " + KEY_LATITUDE + " REAL, "
					+ KEY_LONGITUDE + " REAL, " + KEY_STOREID + " TEXT, " + KEY_GROUPID + " TEXT, " + KEY_CITY + " INT, "
                    + KEY_UPDATED + " DATETIME)");
//			ContentValues initialValues = getEmptyValues();
//			for(int i = 0; i < emptyLinesNumber; i++)
//				//db.execSQL("INSERT INTO " + TABLE_NAME + " (" + KEY_NAME + ") VALUES ('" + emptyLinesName + "')");
//				db.insert(TABLE_NAME, null, initialValues);
			db.execSQL("CREATE TABLE " + VERSION_TABLE_NAME + " (" + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				KEY_CITY + " INTEGER NOT NULL, " + KEY_VERSION + " DATETIME)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + VERSION_TABLE_NAME);
			onCreate(db);
		}
		
	}

	/**
	 * Searches for a store with the given attribute value
	 * @param key The name of the attribute to search by
	 * @param value The value of the attribute to search for
	 * @return A store instance or null if the store wasn't found
	 */
	public Store findByAttribute(String key, String value) {
		Cursor cursor = db.query(TABLE_NAME, getStoreFields(), key + "=?", new String[] {value}, null, null, null);
		Store store = null;
		if(cursor != null && cursor.moveToFirst()) {
			store = createStoreFromCursor(cursor);
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return store;
	}
}
