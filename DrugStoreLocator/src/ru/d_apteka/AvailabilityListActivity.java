package ru.d_apteka;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class AvailabilityListActivity extends ItemRelatedActivity implements
		OnTaskCompleteListener<Void> {
	private AvailabilityAdapter listViewAdapter;
	private ListView listViewResult;
	/** Used to ask user to wait while the stores are loading */
	private ProgressDialog waitDialog;
	private MyApp app;
    private View emptyView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.item_availability);
        emptyView = findViewById(R.id.emptyAvailabilitiesListView);

		listViewResult = (ListView) findViewById(R.id.listViewResult);

		waitDialog = new ProgressDialog(this);
		// Show current message in progress dialog
		waitDialog.setMessage(getString(R.string.loadingStoresDialog));
		waitDialog.setOnCancelListener(new OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				// If the user pressed Back button while waiting, we should go
				// back from this activity
				finish();
			}
		});
		app = (MyApp) getApplicationContext();

		ReadStoresTask task = app.getReadStoresTask();
		if (task.getStatus() != AsyncTask.Status.FINISHED) {
			task.setOnTaskFinishListener(this);
			if (!waitDialog.isShowing()) {
				waitDialog.show();
			}
		} else {
			SavedState state = (SavedState) getLastNonConfigurationInstance();
			setAvailabilityAdapter(state);
		}

		this.setTitle(mItem.getName());

	}

	private void setAvailabilityAdapter() {
		setAvailabilityAdapter(null);
	}

	private void setAvailabilityAdapter(SavedState state) {
		ArrayList<ItemInStore> items = new ArrayList<ItemInStore>();
		MedicineItem rootItem = mItem;
		int page = 0;
		if (state != null) {
			items = state.items;
			rootItem = state.rootItem;
			page = state.curPage;
		}
		listViewAdapter = new AvailabilityAdapter(this, mSmartLocationTeller,
				rootItem, R.layout.availability_item, R.id.availability_text1,
				R.id.availability_text2, R.id.availability_price,
				R.id.availability_dist, R.layout.progress_item, items, page);
		listViewResult.setAdapter(listViewAdapter);
		listViewResult.setOnItemClickListener(listViewItemClickListener);
        listViewAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                if(listViewAdapter.getCount() > 0 || !listViewAdapter.loadingFinished()) {
                    emptyView.setVisibility(View.GONE);
                } else {
                    emptyView.setVisibility(View.VISIBLE);
                }
                super.onChanged();
            }
        });
	}

	private OnItemClickListener listViewItemClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			ItemInStore itemClicked = (ItemInStore) arg0
					.getItemAtPosition(arg2);
			// TODO Show a store on map and open its description
			if (itemClicked != null)
				openMap(itemClicked.getStore());
		}
	};

	/**
	 * Opens map tab and positions the map on the selected store
	 * 
	 * @param store
	 *            Store to position to
	 */
	private void openMap(Store store) {
		Intent intent = new Intent(this, MapActivity.class);
		intent.putExtra("storeId", store.getId());
		startActivity(intent);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onTaskComplete(Void result) {
		setAvailabilityAdapter();
		if (waitDialog != null && waitDialog.isShowing()) {
			waitDialog.dismiss();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.app.Activity#onRetainNonConfigurationInstance()
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		if (listViewAdapter != null) {
			SavedState state = new SavedState();
			state.items = listViewAdapter.getItems();
			state.rootItem = listViewAdapter.getRootItem();
			state.curPage = listViewAdapter.getCurPage();
			return state;
		}
		return null;
	}

	public class SavedState {
		public ArrayList<ItemInStore> items;
		public MedicineItem rootItem;
		public int curPage;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
//		mSmartLocationListener.onStart();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		mSmartLocationTeller.onStop();
	}
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.ItemRelatedActivity#_onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	protected void _onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super._onCreateContextMenu(menu, v, menuInfo);
		if(mExtras.getBoolean("showDetailsBtn") == true)
			menu.add(Menu.NONE, 2, 2, R.string.item_details);
	}
	
	/* (non-Javadoc)
	 * @see ru.d_apteka.ItemRelatedActivity#_onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	protected boolean _onContextItemSelected(MenuItem item) {
		if(item.getItemId() == 2) {
			Intent intent = new Intent(mActivity, DetailsActivity.class);
			intent.putExtra("item", mItem);
			startActivity(intent);
		} else {
			return false;
		}
		return true;
	}
}
