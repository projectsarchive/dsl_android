package ru.d_apteka;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import ru.d_apteka.StoreInformation.WorkingDay;
import ru.d_apteka.StoreInformation.WorkingDay.WorkingHour;

public class GetStoreInformationTask extends ApiGetterTask<String, StoreInformation> {
	private Store mStore;
	private String mStoreSid;

	public GetStoreInformationTask(MyApp app) {
		super(app);
	}

	@Override
	protected String getPath() {
        if(mStoreSid == null) {
            throw new IllegalArgumentException("Store sid not defined for getting schedule");
        }
		return MyApp.server + "/stores/" + mStoreSid + "/schedule";
	}
	
	@Override
	protected boolean beforeRun(String... params) {
		mStoreSid = params[0];
		StoresDatabaseHelper helper = new StoresDatabaseHelper(app);
		mStore = helper.findByAttribute(StoresDatabaseHelper.KEY_STOREID, mStoreSid);
		helper.close();
		return mStore != null;
	}

	@Override
	protected ArrayList<BasicNameValuePair> getUriParams(String... params) {
		ArrayList<BasicNameValuePair> resultParams = new ArrayList<BasicNameValuePair>();
//		if(mStore != null)
//			resultParams.add(new BasicNameValuePair("id", mStore.getId()));
		return resultParams;
	}

	@Override
	protected StoreInformation processOutput(String result, String... params) {
		if(mStore == null)
			return null;
		StoreInformation info = new StoreInformation(mStore);
		ArrayList<WorkingDay> workingDays = new ArrayList<StoreInformation.WorkingDay>();
		try {
			JSONObject res = new JSONObject(result);
			JSONArray jArray = res.getJSONArray("response");
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject json_data = jArray.getJSONObject(i);
				
				JSONArray intervals = json_data.getJSONArray("intervals");
				WorkingDay workingDay = new WorkingDay();
				workingDay.dayId = json_data.getInt("day");
				for(int j = 0; j < intervals.length(); j++) {
					JSONObject hour = intervals.getJSONObject(j);
					WorkingHour workingHour = new WorkingHour();
					workingHour.from = hour.getInt("from");
					workingHour.to = hour.getInt("to");
					workingDay.hours.add(workingHour);
				}
                Calendar calendar = Calendar.getInstance();
                int today = 1 + (7 + calendar.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY) % 7;
                workingDay.currentDay = today == workingDay.dayId;
				workingDays.add(workingDay);
			}
		} catch (JSONException e) {
			Log.e(this.toString(), "Error parsing data " + e.toString());
			return null;
		}
		info.setWorkingDays(workingDays);
		return info;
	}

    @Override
    protected int getInternetErrorMessage() {
        return R.string.internet_error_store_desc;
    }
}
