package ru.d_apteka;

import ru.d_apteka.SmartLocationTeller.SmartLocationListener;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

/**
 * The activity gets the last location. If the location is not valid it starts
 * the location listener. When the new valid location received the location
 * listener is stopped.
 * 
 * @author Sidelnikov Stanislav <sidelnikov.stanislav@gmail.com>
 */
public class LocationCheckingActivity extends Activity {
	protected SmartLocationTeller mSmartLocationTeller;
	protected LatLng mLastLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSmartLocationTeller = new SmartLocationTeller(this);
		mSmartLocationTeller.setNeedGPS(true);
		mLastLocation = mSmartLocationTeller.getCurrentPosition();
		if (mLastLocation == null) {
			mSmartLocationTeller.setListener(new SmartLocationListener() {

				public void onLocationChanged(Location newLocation) {
					mLastLocation = mSmartLocationTeller.getCurrentPosition();
					if (mLastLocation != null) {
						mSmartLocationTeller.onStop();
					}
				}
			});
			mSmartLocationTeller.onStart();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mSmartLocationTeller.onStop();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(mSmartLocationTeller.getCurrentPosition() == null && !mSmartLocationTeller.isListening())
			mSmartLocationTeller.onStart();
	}
}
