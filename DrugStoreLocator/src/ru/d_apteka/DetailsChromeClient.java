package ru.d_apteka;

import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;

public class DetailsChromeClient extends WebChromeClient {

	@Override
	public void onConsoleMessage(String message, int lineNumber,
			String sourceID) {
		Log.v("ChromeClient", "invoked: onConsoleMessage() - " + sourceID
				+ ":" + lineNumber + " - " + message);
		super.onConsoleMessage(message, lineNumber, sourceID);
	}

	@Override
	public boolean onConsoleMessage(ConsoleMessage cm) {
		Log.v("ChromeClient",
				cm.message() + " -- From line " + cm.lineNumber() + " of "
						+ cm.sourceId() + ". " + cm.toString());
		return true;
	}
}