package ru.d_apteka;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask.Status;

/**
 * Adapter used to fetch results from the server using
 * {@link MedicineItemFetcher} by portions (pages). The page size is stored in
 * {@link ItemDynamicViewAdapter.TaskParams#pageSize}.
 * 
 * @author stas
 * 
 */

public class ItemDynamicViewAdapter
		extends
		DynamicViewAdapter<MedicineItem, FullSearchTask, ArrayList<MedicineItem>> {
	/**
	 * If the loading process is finished It is set to true when the fetching
	 * results from the server returns an empty set
	 */
	private boolean loadingFinished = false;
	private ArrayList<MedicineItem> items = new ArrayList<MedicineItem>();
	private String searchTerm = "";
	FullSearchTask currentTask;
	private int curPage = 0;
	private Context context;

	public ItemDynamicViewAdapter(Context context, int resource,
			int textViewResourceId, int loadingViewId) {
		super(context, resource, textViewResourceId, loadingViewId);
		this.context = context;
	}

	/**
	 * 
	 */
	public ItemDynamicViewAdapter(Context context, int resource,
			int textViewResourceId, int loadingViewId,
			ArrayList<MedicineItem> defItems, String term, int page) {
		this(context, resource, textViewResourceId, loadingViewId);
		items = defItems;
		this.count = items.size();
		searchTerm = term;
		curPage = page;
		notifyDataSetChanged();
	}

	public ItemDynamicViewAdapter(Context context, int textViewResourceId,
			int loadingViewId) {
		super(context, textViewResourceId, loadingViewId);
		this.context = context;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#getData()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void getData() {
		if ("".equals(searchTerm))
			return;
		if (currentTask == null || currentTask.getStatus() != Status.RUNNING
				|| currentTask.isReallyCancelled()) {
			FullSearchTask task = new FullSearchTask(this.context);
			task.setOnTaskCompleteListener(this);
			DynamicTaskParams<String> params = new DynamicTaskParams<String>(
					searchTerm, curPage);
			// if(currentTask == null || !currentTask.isReallyCancelled())
			curPage++;
			task.execute(params);
			currentTask = task;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#getItem(int)
	 */
	@Override
	public MedicineItem getItem(int position) {
		MedicineItem item = null;
		if (position < count)
			item = items.get(position);
		return item;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#loadingFinished()
	 */
	@Override
	public boolean loadingFinished() {
		return "".equals(searchTerm) || loadingFinished;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see ru.d_apteka.DynamicViewAdapter#onTaskComplete(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public void onTaskComplete(ArrayList<MedicineItem> result) {
		int size = 0;
		if (result != null) {
			size = result.size();
			items.addAll(result);
			count += size;
		}
		loadingFinished = size == 0;
		notifyDataSetChanged();
		currentTask = null;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
		curPage = 0;
		count = 0;
		loadingFinished = false;
		items.clear();
		if (currentTask != null && currentTask.getStatus() == Status.RUNNING)
			currentTask.cancel(true);
		currentTask = null;
		if (searchTerm != "") {
			getData();
		}
		notifyDataSetChanged();
	}

	/**
	 * Brings the adapter to it's initial state
	 * 
	 * @param searchTerm
	 *            The new search term to show results for
	 */
	public void resetAdapter() {
		setSearchTerm("");
	}

	/**
	 * @return the items
	 */
	public ArrayList<MedicineItem> getItems() {
		return items;
	}

	/**
	 * @return the curPage
	 */
	public int getCurPage() {
		return curPage;
	}

	/**
	 * 
	 */
	public void cancelCurrentTask() {
		if (currentTask != null && currentTask.getStatus() == Status.RUNNING) {
			currentTask.cancel(true);
			curPage--;
			loadingFinished = false;
		}
	}

}