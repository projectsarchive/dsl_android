package ru.d_apteka;

import ru.d_apteka.StringPrompt.StringPromptAnswerListener;
import android.content.Context;
import android.widget.Toast;

public class ListsEditor {

	private Context mContext;
	private ListsEditedListener mListener;
	private ItemsList mList;

	public ListsEditor(Context context) {
		mContext = context;
	}

	public void setListsAddedListener(ListsEditedListener listener) {
		mListener = listener;
	}

	public void editList() {
		editList(null);
	}

	public void editList(MedicineItem item) {
		StringPrompt prompt = new StringPrompt(mContext);
		prompt.setSingleLine(true);
		int titleId = mList == null ? R.string.lists_adder_prompt_title_adding : R.string.lists_adder_prompt_title;
		prompt.setTitle(mContext.getString(titleId));
		prompt.setMessage(mContext
				.getString(R.string.lists_adder_prompt_message));
		prompt.setTag(item);
		if (mList != null) {
			prompt.setValue(mList.getName());
		}
		prompt.setListener(new StringPromptAnswerListener() {

			@Override
			public boolean onAnswer(StringPrompt prompt) {
				if (prompt.getValue().equals(""))
					return false;
				if (mList == null)
					mList = new ItemsList();
				mList.setName(prompt.getValue());
				UsersDatabaseHelper helper = new UsersDatabaseHelper(mContext);
				boolean result = helper.saveList(mList);
				if (!result) {
					Toast.makeText(mContext,
							R.string.lists_adder_error_adding_list,
							Toast.LENGTH_LONG).show();
				}
				if (mListener != null) {
					MedicineItem item = (MedicineItem) prompt.getTag();
					mListener.onListEdited(mList, item);
				}
				return true;
			}
		});
		prompt.ask();
	}

	public void setListToEdit(ItemsList list) {
		mList = list;
	}

	public static abstract class ListsEditedListener {
		public abstract void onListEdited(ItemsList list, MedicineItem item);
	}
}
