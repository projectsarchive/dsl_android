/**
 * 
 */
package ru.d_apteka;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author stas
 * 
 */
public class MainMenuAdapter extends BaseAdapter {
	private Context context;
	private Typeface tf;
    private final LayoutInflater mInflater;


    public MainMenuAdapter(Context context) {
		this.context = context;

        /*TypedArray a = context.getTheme().obtainStyledAttributes(R.style.mytheme, new int[] {android.R.attr.textSize});
        int attributeResourceId = a.getResourceId(0, 0);*/

		tf = Typeface.createFromAsset(context.getAssets(),
				context.getString(R.string.mainmenu_font));
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		return 4;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	public MainMenuItem getItem(int position) {
		Resources res = context.getResources();
		MainMenuItem item = new MainMenuItem();
		Intent intent = new Intent();

		switch (position) {
			case 0:
				// Item availability
				intent.setClass(context, SearchActivity.class);
				intent.setAction("ACTION_AVAILABILITY");

				item.title = context.getString(R.string.item_availability);
				item.icon = res.getDrawable(R.drawable.icon_availability);
				item.intent = intent;
				break;
			case 1:
				// Stores on map
				intent.setClass(context, MapActivity.class);

				item.title = context.getString(R.string.all_stores);
				item.icon = res.getDrawable(R.drawable.icon_pharmacies);
				item.intent = intent;
				break;
			case 2:
				// Scanner

				// intent.setClass(context, SearchActivity.class);
				// intent.putExtra("startScanner", true);
				// item.function = new ICallable() {
				//
				// public void onCall(MainMenuItem item) {
				// // BarcodeIntegrator integrator = new
				// BarcodeIntegrator(item.activity);
				// //
				// integrator.initiateScan(BarcodeIntegrator.ONE_D_CODE_TYPES);
				//
				// Intent intent1 = new Intent("ru.d_apteka.SCAN");
				// intent1.putExtra("SCAN_MODE", "ONE_D_MODE");
				// item.activity.startActivityForResult(intent1, 0);
				// }
				// };

				intent.setAction("ru.d_apteka.SCAN");
				intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
				item.intent = intent;

				item.title = context.getString(R.string.scanner);
				item.icon = res.getDrawable(R.drawable.icon_bar_code);
				break;
			case 3:
				// Items Lists
				intent.setClass(context, ListsListActivity.class);

				item.title = context.getString(R.string.menu_items_lists);
				item.icon = res.getDrawable(R.drawable.icon_description);
				item.intent = intent;
				break;
//			case 4:
//				// Settings
//				intent.setClass(context, SettingsActivity.class);
//
//				item.title = context.getString(R.string.settings);
//				item.icon = res.getDrawable(R.drawable.ic_menu_settings);
//				item.intent = intent;
//				break;
			default:
				break;
		}

		return item;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 *      android.view.ViewGroup)
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView tv;
		final MainMenuItem data = getItem(position);

		if (convertView == null) {

			//tv = new TextView(context.getApplicationContext());
            tv = (TextView) mInflater.inflate(R.layout.mainmenu_adapter_item,
                    parent, false);
			tv.setGravity(Gravity.CENTER);
			tv.setTypeface(tf);

		} else {
			tv = (TextView) convertView;
		}

		Drawable icon = data.icon;
		CharSequence title = data.title;

		tv.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);
		tv.setText(title);
		tv.setTag(data);

		return tv;
	}

}
