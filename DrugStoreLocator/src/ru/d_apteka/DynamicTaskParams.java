/**
 * 
 */
package ru.d_apteka;

class DynamicTaskParams<Param> {
	private Param params;
	private int page = 0;
	/** Number of items to be fetched at a time */
	final int pageSize = 20;

	/**
	 * @return the searchTerm
	 */
	public Param getParams() {
		return params;
	}

	/**
	 * @param searchTerm
	 *            the searchTerm to set
	 */
	public void setParams(Param params) {
		this.params = params;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	public DynamicTaskParams(Param searchTerm, int page) {
		this.params = searchTerm;
		this.page = page;
	}

	public DynamicTaskParams(Param searchTerm) {
		this.params = searchTerm;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

}