package ru.d_apteka;

/**
 * Represents an item at the store
 * @author stas
 *
 */
public class ItemInStore {
	private Store store;
	private MedicineItem item;
	private Double quantity = 0.0;
	private Double price = 0.0;
	private Double distance = 0.0;
	
	/**
	 * @param store Store where the item is found
	 * @param item Item that is found
	 * @param quantity Quantity of the item found at the store
	 * @param price The price of the item
	 */
	public ItemInStore(Store store, MedicineItem item, Double quantity, Double price) {
		super();
		this.store = store;
		this.item = item;
		this.quantity = quantity;
		this.price = price;
	}
	
	@Override
	public String toString() {
		return store.getAddress();
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public MedicineItem getItem() {
		return item;
	}

	public void setItem(MedicineItem item) {
		this.item = item;
	}

	/**
	 * @return the quantity
	 */
	public Double getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
}
