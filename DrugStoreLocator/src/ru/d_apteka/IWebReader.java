/**
 * 
 */
package ru.d_apteka;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 *
 */
public interface IWebReader {
	public String readWebResource(String path, ArrayList<BasicNameValuePair> params) throws IOException;
//	public String readWebResource(String path, ArrayList<BasicNameValuePair> params, Boolean compression);
}