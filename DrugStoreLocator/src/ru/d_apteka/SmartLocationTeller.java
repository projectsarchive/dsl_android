package ru.d_apteka;

import java.util.Date;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * The class keeps the current user location up to date and returns it when
 * requested. If the location updates weren't recieved at the moment of request,
 * the lastKnownLocation is used. You should set the
 * {@link SmartLocationListener} interface to be notified of the real location
 * updates.
 * 
 * @author Sidelnikov Stanislav <sidelnikov.stanislav@gmail.com>
 */
public class SmartLocationTeller implements LocationListener {
	protected LocationManager mLocationManager;
	private Location mLastLocation;
    private static final int MINUTES = 1 * 60 * 1000;
	private Context mContext;
	/**
	 * Determines if the activity needs the accurate positioning (GPS)
	 */
	private boolean mNeedGPS = false;
	private SmartLocationListener mListener;
	/**
	 * Maximum time difference in milliseconds to consider the cached location
	 * invalid. Used in {@link #getCurrentLocation()} and
	 * {@link #getCurrentPosition()}
	 */
	private final long maximumTimeDifference = 10 * MINUTES;
	private boolean mListening = false;

	public SmartLocationTeller(Context context) {
		mContext = context;
		mLocationManager = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);
	}

	public void onStart() {
		Log.d("GeoLocation", "onStart");
		// Update time for the location changes - 60 seconds
		int updateTime = 60000;
		int updateDistance = 100;
		mLocationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, updateTime, updateDistance,
				this);
		if (mNeedGPS
				&& mLocationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER))
			mLocationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, updateTime, updateDistance,
					this);
		mListening = true;
	}

	public void onStop() {
		Log.d("GeoLocation", "onStop");
		mLocationManager.removeUpdates(this);
		mListening = false;
	}

	public void onLocationChanged(Location location) {
		if (isBetterLocation(location, mLastLocation)) {
			mLastLocation = location;
			if (mListener != null) {
				Log.d("GeoLocation", String.format("Location changed: %f, %f", mLastLocation.getLongitude(), mLastLocation.getLatitude()));
				mListener.onLocationChanged(mLastLocation);
			}
		}
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public LatLng getCurrentPosition() {
		Location result = getCurrentLocation();
        LatLng position = null;
		if (result != null) {
			position = getGeopoint(result);
		}
		return position;
	}

	public Location getCurrentLocation() {
		Location result = getCurrentExactLocation();
        if (result == null) {
            result = MyApp.getCurrentCityLocation(mContext);
        }
		Log.d("GeoLocation", "getCurrentLocation: " + (result == null ? "false" : "true"));
		return result;
	}

    public boolean isLocationAccurate() {
        return getCurrentExactLocation() != null;
    }

    protected Location getCurrentExactLocation() {
        Location result = mLastLocation;
        if (result == null) {
            if (mLocationManager != null) {
                result = mLocationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Location gpsResult = mLocationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (gpsResult != null && isBetterLocation(gpsResult, result))
                    result = gpsResult;
            }
        }
        long difference = -1;
        if (result != null) {
            Date now = new Date();
            difference = (now.getTime() - result.getTime());
            if(difference > maximumTimeDifference)
                result = null;
        }
        return result;
    }

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @link http://developer.android.com/intl/ru/training/basics/location/
	 *       currentlocation.html
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		if (location == null) {
			return false;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > 2 * MINUTES;
		boolean isSignificantlyOlder = timeDelta < -2 * MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	public boolean isNeedGPS() {
		return mNeedGPS;
	}

	public void setNeedGPS(boolean mNeedGPS) {
		this.mNeedGPS = mNeedGPS;
	}

	public void setListener(SmartLocationListener listener) {
		Log.d("GeoLocation", "Setting listener");
		this.mListener = listener;
	}

	public static LatLng getGeopoint(Location location) {
		return new LatLng(location.getLatitude(), location.getLongitude());
	}

	public static interface SmartLocationListener {
		public void onLocationChanged(Location newLocation);
	}

	public boolean isListening() {
		return mListening ;
	}
}
