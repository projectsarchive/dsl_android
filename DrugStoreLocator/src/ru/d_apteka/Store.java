package ru.d_apteka;

import com.google.android.gms.maps.model.LatLng;

/**
 * Represents a store on the map
 * @author stas
 *
 */
public class Store extends JSONReady {
	
	public static enum Day{Mon, Tue, Wed, Thu, Fri, Sat, Sun}
		
	private String Name;
	private String Address;
	private LatLng Point;
	private String Rubrics;
	/**
	 * An id of this store (storeId field in the database)
	 * @see StoresDatabaseHelper#getStoreValues
	 */
	private String Id;
	private String groupId;
	private int AlvikId;
	private int City;
//	private Map<Day, int[]> WorkingDays;
	
	public Store() {
		
	}
	
	public Store(String latitude, String longitude, String name, String address, Integer city){
		setPoint(latitude, longitude);
		setName(name);
		setAddress(address);
		setCity(city);
	}
	
	public Store(Double latitude, Double longitude, String name, String address, Integer city){
		setPoint(latitude, longitude);
		setName(name);
		setAddress(address);
		setCity(city);
	}
	
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public LatLng setPoint(String latitude, String longitude){
    	String coordinates[] = {latitude, longitude};
        double lat = Double.parseDouble(coordinates[0]);
        double lng = Double.parseDouble(coordinates[1]);

        Point = new LatLng(lat, lng);
        return Point;
    }
	
	public LatLng setPoint(Double latitude, Double longitude){
    	Point = new LatLng(latitude, longitude);
        return Point;
    }
	
	public void setPoint(LatLng point){
		Point = point;
	}
	
	public LatLng getPoint(){
		return Point;
	}

	public String getRubrics() {
		return Rubrics;
	}

	public void setRubrics(String rubrics) {
		Rubrics = rubrics;
	}

	/**
	 * @return Id of this store (storeId field in the database)
	 */
	public String getId() {
		return Id;
	}

	/**
	 * Sets the id of this store (storeId field in the database)
	 * @param id A new id of the store
	 */
	public void setId(String id) {
		Id = id;
	}

//	public Map<Day, int[]> getWorkingDays() {
//		return WorkingDays;
//	}
//
//	public void setWorkingDays(Map<Day, int[]> workingDays) {
//		WorkingDays = workingDays;
//	}
//	
//	public int[] getWorkingHours(Day day){
//		if(WorkingDays.containsKey(day))
//			return WorkingDays.get(day);
//		else
//			return null;
//	}
//	
//	public void setWorkingHours(Day day, int[] hours){
//		WorkingDays.put(day, hours);
//	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public int getAlvikId() {
		return AlvikId;
	}

	public void setAlvikId(int alvikId) {
		AlvikId = alvikId;
	}

	public int getCity() {
		return City;
	}

	public void setCity(int city) {
		City = city;
	}
}