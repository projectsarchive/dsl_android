package ru.d_apteka;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class StoreDetailsActivity extends MyActivity implements OnTaskCompleteListener<StoreInformation> {
	TextView title;
	TextView details;
    String mStoreAddress;
    Button refreshButton;
	private String mStoreSid; 
	private ProgressDialog mDialog;
	private WebView storeDetailsWebView;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details);
		
		title = (TextView)findViewById(R.id.title);
		details = (TextView)findViewById(R.id.details);
		storeDetailsWebView = (WebView) findViewById(R.id.storeDetailsWebView);
		storeDetailsWebView.setWebChromeClient(new DetailsChromeClient());
//		storeDetailsWebView.setBackgroundColor(getResources().getColor(R.color.transparent));
		storeDetailsWebView.setBackgroundColor(0);
		storeDetailsWebView.setPadding(0, 0, 0, 0);
		storeDetailsWebView.getSettings().setJavaScriptEnabled(true);
		storeDetailsWebView.getSettings().setDomStorageEnabled(true);
		storeDetailsWebView.getSettings().setDefaultTextEncodingName("utf-8");
		storeDetailsWebView.addJavascriptInterface(new JSCommon(), "JSCommon");

        refreshButton = (Button)findViewById(R.id.refresh_store_details);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStoreInfo();
            }
        });
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			title.setText(extras.getString("title"));
            mStoreAddress = extras.getString("details");
//			mStore = (Store) extras.getSerializable("store");
			mStoreSid = extras.getString("storeSid");
		}
		
		//this.setTitle(title.getText());

		loadUrl("file:///android_asset/www/store_details.html");
		
		mDialog = new ProgressDialog(mActivity);
		mDialog.setMessage(getString(R.string.loading_common));

        getStoreInfo();
	}

    private void getStoreInfo() {
        mDialog.show();
        MyApp app = (MyApp) getApplication();
        GetStoreInformationTask getInformationTask = new GetStoreInformationTask(app);
        getInformationTask.setOnTaskCompleteListener(this);
        getInformationTask.execute(mStoreSid);
    }

	public void onTaskComplete(StoreInformation result) {
		if(mDialog != null && mDialog.isShowing())
			mDialog.dismiss();
		String text = mStoreAddress + "\n";
		if(result == null) {
			text += getString(R.string.nothing_found);
            details.setText(text);
            hideWebView();
		} else {
			String json = result.getJSON();
//			Log.d("Reflection", json);
			loadUrl("javascript:fillData(" + json + ")");
			// If the above doesn't work:
//			Gson son = new GsonBuilder().serializeNulls().create();
//			String gson = son.toJson(result);
//			Log.d("Reflection", "My:\n" + my);
//			Log.d("Reflection", "GSON:\n" + gson);
//			text += "Working days count: " + Integer.toString(result.getWorkingDays().size());
            showWebView();
        }
	}

    protected void setWebViewVisible(boolean visibility) {
        storeDetailsWebView.setVisibility(visibility ? View.VISIBLE : View.GONE);
        title.setVisibility(visibility ? View.GONE : View.VISIBLE);
        details.setVisibility(visibility ? View.GONE : View.VISIBLE);
        refreshButton.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    protected void showWebView() {
        setWebViewVisible(true);
    }

    protected void hideWebView() {
        setWebViewVisible(false);
    }

	private void loadUrl(final String in) {
		Handler handler = new Handler();
		handler.post(new Runnable() {
			public void run() {
				storeDetailsWebView.loadUrl(in);
			}
		});
	}
}
