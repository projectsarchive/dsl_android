/**
 * 
 */
package ru.d_apteka;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 *
 */
public class GetDetailsTask extends ApiGetterTask<MedicineItem, String> {

	/**
	 * @param app
	 */
	public GetDetailsTask(MyApp app) {
		super(app);
	}

	/**
	 * {@inheritDoc}
	 * @see ru.d_apteka.ApiGetterTask#getPath()
	 */
	@Override
	protected String getPath() {
		return MyApp.server + "/items/:itemid/description";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ArrayList<BasicNameValuePair> getUriParams(MedicineItem... params) {
		ArrayList<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		MedicineItem item = params[0];
		values.add(new BasicNameValuePair("itemid", item.getId().toString()));
		return values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String processOutput(String result,
			MedicineItem... params) {
        JSONObject res;

		try {
            res = new JSONObject(result);
//			JSONObject desc = res.getJSONObject("result");
//			resultDetails.setShortHtmlDescription(desc.getString("shortDesc"));
//			resultDetails.setExtraHtmlDescription(desc.getString("extraDesc"));
		} catch (JSONException e) {
			Log.e(this.toString(), "Error parsing data " + e.toString());
			return null;
		}
		
		return res.toString();
	}

}
