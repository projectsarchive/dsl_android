/**
 * 
 */
package ru.d_apteka;

import android.net.TrafficStats;

/**
 * Method for calculating traffic of the current application.
 * @author stas
 *
 */
public class TrafficStatistics {
	private int uid;
	private long trafficTxStart;
	private long trafficRxStart;

	public TrafficStatistics() {
		super();
		this.uid = android.os.Process.myUid();
		trafficTxStart = getCurUidTxTraffic(uid);
		trafficRxStart = getCurUidRxTraffic(uid);
	}
	
	/**
	 * @return Total (recieved and sent) traffic by this app from the moment the class was instantiated
	 */
	public long getMyTxTraffic() {
		return getCurUidTxTraffic(uid) - trafficTxStart;
	}
	
	private long getCurUidTxTraffic(int uid) {
		long traff = TrafficStats.getUidTxBytes(uid);
		if(traff == TrafficStats.UNSUPPORTED)
			traff = TrafficStatsFile.getUidTxBytes(uid);
		return traff;
	}
	
	private long getCurUidRxTraffic(int uid) {
		long traff = TrafficStats.getUidRxBytes(uid);
		if(traff == TrafficStats.UNSUPPORTED)
			traff = TrafficStatsFile.getUidRxBytes(uid);
		return traff;
	}
	
	/**
	 * @return Recieved traffic by this app from the moment the class was instantiated
	 */
	public long getMyRxTraffic() {
		return getCurUidRxTraffic(uid) - trafficRxStart;
	}
}
