package ru.d_apteka;

import java.util.ArrayList;

import ru.d_apteka.DetailsActivity.DetailsViewerClient;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.webkit.WebView;

import com.google.android.gms.maps.model.LatLng;

public class MyApp extends Application {
	private ArrayList<Store> stores = new ArrayList<Store>();
	public static final String server = "http://d-apteka.ru/api/1";
	// public static final String server = "http://10.0.2.2:8080";
	private LatLng lastLocation;
	private ReadStoresTask readStoresTask;
	Context context;
	TrafficStatistics traffic;
	private IWebReader webReader = new WebReader();
	public WebView mDetailsWebView;
	public DetailsViewerClient detailsViewerClient;

	@Override
	public void onCreate() {
		super.onCreate();
		context = this;
		// Start counting the traffic
		traffic = new TrafficStatistics();
		// readStoresTask = new ReadStoresTask(this);
		ReadAllStores();
	}

	public static Integer getCurrentCity(Context context) {
		// TODO Return the correct city id
//		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//		prefs.getInt("city_id", 1);
		return 1;
	}

    public static Location getCurrentCityLocation(Context context) {
        // TODO Return correct city location
        Location location = new Location("");
        // Chelyabinsk location
        location.setLatitude(55.1544400);
        location.setLongitude(61.4297200);
        return location;
    }
	
	public Integer getCurrentCity() {
		return MyApp.getCurrentCity(getApplicationContext());
	}

	public ArrayList<Store> getStoresList() {
		return stores;
	}

	/**
	 * @param mStoreId
	 * @return
	 */
	public ArrayList<Store> getStoresList(String mStoreId) {
		if(mStoreId == null || mStoreId.equals("")) {
			return getStoresList();
		}
		ArrayList<Store> result = new ArrayList<Store>();
		for (Store store : stores) {
			if(store.getId().equals(mStoreId)) {
				result.add(store);
				break;
			}
		}
		return result;
	}
	
	/**
	 * Returns the store in the current city by id
	 * 
	 * @param id
	 *            Id of the store
	 * @return
	 */
	public Store getStoreById(String id) {
		for (int i = 0; i < stores.size(); i++)
			if (stores.get(i).getId().equals(id))
				return stores.get(i);
		return null;
	}

	public void ReadAllStores() {
		if (readStoresTask == null
				|| readStoresTask.getStatus() != AsyncTask.Status.RUNNING
				|| readStoresTask.isCancelled()) {
			stores.clear();
			readStoresTask = new ReadStoresTask(this, webReader);
			readStoresTask.execute();
		}
	}

	public LatLng getLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(LatLng lastLocation) {
		this.lastLocation = lastLocation;
	}

	/**
	 * @return GeoPoint of the current user location or null if it is
	 *         unavailable
	 */
	public LatLng getCurrentLocation() {
		// TODO Return the real coordinates
        LatLng p = new LatLng(55.188639, 61.277165);
		return p;
	}

	/**
	 * @return
	 */
	public TrafficStatistics getTrafficStatistics() {
		return traffic;
	}

	/**
	 * @param stores
	 *            the stores to set
	 */
	public void setStores(ArrayList<Store> stores) {
		this.stores = stores;
	}

	public ReadStoresTask getReadStoresTask() {
		return readStoresTask;
	}

	/**
	 * @return the webReader
	 */
	public IWebReader getWebReader() {
		return webReader;
	}

	/**
	 * @param webReader
	 *            the webReader to set
	 */
	public void setWebReader(IWebReader webReader) {
		this.webReader = webReader;
	}

}
