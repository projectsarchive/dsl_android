package ru.d_apteka;

import java.util.ArrayList;


public class ItemsList {
	private long mId;
	private String mName;
	private ArrayList<MedicineItem> mItems;
	public ItemsList() {
		mItems = new ArrayList<MedicineItem>();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return mId;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.mId = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return mName;
	}
	/**
	 * @param string the name to set
	 */
	public void setName(String name) {
		this.mName = name;
	}
	/**
	 * @return the items
	 */
	public ArrayList<MedicineItem> getItems() {
		return mItems;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<MedicineItem> items) {
		this.mItems = items;
	}
	public boolean hasItem(MedicineItem mItem) {
		return mItems.contains(mItem);
	}
	
}
