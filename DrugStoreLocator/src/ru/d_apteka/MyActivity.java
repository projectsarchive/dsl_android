package ru.d_apteka;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;

public class MyActivity extends LocationCheckingActivity {

	private TextView mTitleTextView;
	protected Activity mActivity;
	protected Typeface mTypeface;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = this;
		mTypeface = Typeface.createFromAsset(getAssets(),
				"fonts/MyriadPro-Regular.otf");
	}

	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);
		setTitleTextView();
		if (mTitleTextView != null)
			mTitleTextView.setText(title);
	}

	@Override
	public void setTitle(int titleId) {
		super.setTitle(titleId);
		setTitleTextView();
		if (mTitleTextView != null)
			mTitleTextView.setText(titleId);
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);

		prepareActivity();
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		prepareActivity();
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		prepareActivity();
	}

	private void prepareActivity() {
		mTitleTextView = (TextView) findViewById(R.id.titleText);
		if (mTitleTextView != null) {
			mTitleTextView.setText(getTitle());
			setFontFace();
		}
		// Button homeButton = (Button) findViewById(R.id.titleGoToMain);
		// if (homeButton != null)
		// homeButton.setOnClickListener(new OnClickListener() {
		//
		// public void onClick(View v) {
		// Intent intent = new Intent(mActivity,
		// MainMenuActivity.class);
		// startActivity(intent);
		// }
		// });
	}

	private void setFontFace() {
		if(mTitleTextView != null)
			mTitleTextView.setTypeface(mTypeface);
	}

	private void setTitleTextView() {
		if (mTitleTextView == null) {
			mTitleTextView = (TextView) findViewById(R.id.titleText);
			setFontFace();
		}
	}

    @Override
    protected void onStart() {
        super.onStart();
        gaStartActivity();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gaStopActivity();
    }

    protected EasyTracker gaStartActivity() {
        EasyTracker tracker = EasyTracker.getInstance(this);
        tracker.activityStart(this);
        String currentItemName = getCurrentItemName();
        if(currentItemName != null) {
            tracker.set(Fields.customDimension(2), currentItemName);
        }
        return tracker;
    }

    protected EasyTracker gaStopActivity() {
        EasyTracker tracker = EasyTracker.getInstance(this);
        tracker.activityStop(this);
        return tracker;
    }

    protected String getCurrentItemName() {
        return null;
    }
}
