/**
 * 
 */
package ru.d_apteka;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * @author stas
 *
 */
public class MainMenuItem {
	public Drawable icon;
	public String title;
	public Intent intent;
	public ICallable function;
	public Activity activity;
}
