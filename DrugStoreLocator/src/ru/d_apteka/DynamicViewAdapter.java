/**
 * 
 */
package ru.d_apteka;

import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.SwipeableHiddenView;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author stas
 * 
 */
public abstract class DynamicViewAdapter<Item, Task, Result> extends
        BaseAdapter implements OnTaskCompleteListener<Result> {

    private int loadingViewId;
    /**
     * If the inflated resource is not a TextView, {@link #mFieldId} is used to
     * find a TextView inside the inflated views hierarchy. This field must
     * contain the identifier that matches the one defined in the resource file.
     */
    private int mFieldId = 0;
    private LayoutInflater mInflater;
    /**
     * The resource indicating what views to inflate to display the content of
     * this array adapter.
     */
    private int mResource;
    protected int count;
    protected int threshold = 5;
    protected Typeface mTypeface;

    private HiddenQuickActionSetup mQuickActionSetup;

    public DynamicViewAdapter(Context context, int textViewResourceId,
            int loadingViewId) {
        init(context, textViewResourceId, 0, loadingViewId);

    }

    public DynamicViewAdapter(Context context, int resource,
            int textViewResourceId, int loadingViewId) {
        init(context, resource, textViewResourceId, loadingViewId);

    }

    /**
     * {@inheritDoc}
     * 
     * @see android.widget.Adapter#getCount()
     */
    public int getCount() {
        int result = count;
        if (!loadingFinished() && result == 0)
            result = 1;
        return result;
    }

    /**
     * Runs getting the next portion of information
     */
    protected abstract void getData();

    /**
     * {@inheritDoc}
     * 
     * @see android.widget.Adapter#getItem(int)
     */
    public abstract Item getItem(int position);

    /**
     * {@inheritDoc}
     * 
     * @see android.widget.Adapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * {@inheritDoc}
     * 
     * @see android.widget.Adapter#getView(int, android.view.View,
     *      android.view.ViewGroup)
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position > count - threshold && !loadingFinished()) {
            getData();
        }
        if ((position == count - 1 || count == 0) && !loadingFinished()) {
            convertView = getLoadingView(parent);
        } else if (position <= count - 1) {
            convertView = createViewFromResource(position, convertView, parent,
                    mResource);
        }
        return convertView;
    }

    /**
     * @return true if the loading of data is finished, false otherwise
     */
    public abstract boolean loadingFinished();

    private View createViewFromResource(int position, View convertView,
            ViewGroup parent, int resource) {
        View view;
        TextView text;

        if (convertView == null || convertView.getId() == loadingViewId
                || convertView.getId() < 0) {
            if (mQuickActionSetup == null)
                view = mInflater.inflate(resource, parent, false);
            else {
                view = (SwipeableHiddenView) mInflater.inflate(resource,
                        parent, false);
                ((SwipeableHiddenView) view)
                        .setHiddenViewSetup(mQuickActionSetup);
            }
        } else {
            view = convertView;
        }

        try {
            if (mFieldId == 0) {
                // If no custom field is assigned, assume the whole resource is
                // a TextView
                text = (TextView) view;
            } else {
                // Otherwise, find the TextView field within the layout
                text = (TextView) view.findViewById(mFieldId);
            }
        } catch (ClassCastException e) {
            Log.e("DynamicViewAdapter",
                    "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "DynamicViewAdapter requires the resource ID to be a TextView",
                    e);
        }

        if (convertView == null) {
            text.setTypeface(mTypeface);
        }

        Item item = getItem(position);
        if (item instanceof CharSequence) {
            text.setText((CharSequence) item);
        } else {
            text.setText(item.toString());
        }

        view = onCreatingViewFromResource(view, item);

        return view;
    }

    /**
     * Called when a view filled in with text to let childs add any additional
     * information.
     * 
     * @param view
     *            The view generated for the item
     * @param item
     *            Item the view is generated for
     * @return View after inserting additional information. If method is nor
     *         overriden, the original view
     */
    protected View onCreatingViewFromResource(View view, Item item) {
        return view;
    }

    private View getLoadingView(ViewGroup parent) {
        View view = mInflater.inflate(loadingViewId, parent, false);
        return view;
    }

    private void init(Context context, int resource, int textViewResourceId,
            int loadingViewId) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mFieldId = textViewResourceId;
        this.loadingViewId = loadingViewId;
        this.mTypeface = Typeface.createFromAsset(context.getAssets(),
                "fonts/MyriadPro-Regular.otf");
    };

    public void setQuickActionSetup(HiddenQuickActionSetup quickActionSetup) {
        mQuickActionSetup = quickActionSetup;
    }

    /**
     * {@inheritDoc}
     * <p>
     * It should call <code>notifyDataSetChanged()</code> if the data really
     * changed.
     * </p>
     * 
     * @see ru.d_apteka.OnTaskCompleteListener#onTaskComplete(java.lang.Object,
     *      java.lang.Object)
     * @see BaseAdapter#notifyDataSetChanged()
     */
    public abstract void onTaskComplete(Result result);

}
