/**
 *
 */
package ru.d_apteka;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 */
public class ReadStoresTask extends AsyncTask<String, Void, Void> {


    /**
     *
     */
    private final MyApp myApp;
    public ArrayList<OnTaskCompleteListener<Void>> mTaskCompleteListeners = new ArrayList<OnTaskCompleteListener<Void>>();
    private StoresDatabaseHelper dbh;
    private IWebReader webReader;
    private boolean connectionError = false;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * @param myApp
     */
    ReadStoresTask(MyApp myApp, IWebReader reader) {
        this.myApp = myApp;
        dbh = new StoresDatabaseHelper(myApp.context);
        webReader = reader;
    }

    @Override
    protected Void doInBackground(String... params) {
        publishProgress(new Void[]{});

        // TODO Update stores only periodically
        if (!isCancelled()) {
            Log.d(this.toString(), "Updating database");
            UpdateStoresFromServer();
        }
        if (!isCancelled()) {
            Log.d(this.toString(), "Reading stores");
            ReadAllStoresFromDB();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (isCancelled())
            return;
        super.onPostExecute(result);
        onFinish();
        if (mTaskCompleteListeners.size() > 0) {
            for (OnTaskCompleteListener<Void> listener : mTaskCompleteListeners) {
                listener.onTaskComplete(result);
                mTaskCompleteListeners.remove(listener);
            }
        }
    }

    @Override
    protected void onCancelled() {
        // TODO Let the map activity know if loading map was canceled (back button is pressed)
        super.onCancelled();
        onFinish();
    }

    public void UpdateStoresFromServer() {
        int cityId = myApp.getCurrentCity();
        String path = MyApp.server + "/stores";
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("cityid", Integer.toString(cityId)));
        params.add(new BasicNameValuePair("per_page", Integer.toString(999999)));
        Date lastDate = dbh.getLastUpdate(cityId);
        if(lastDate != null) {
            params.add(new BasicNameValuePair("date", dateFormat.format(lastDate)));
        }
        String response;
        try {
            response = webReader.readWebResource(path, params);
        } catch (IOException e1) {
            connectionError = true;
            return;
        }
        try {
            JSONObject obj = new JSONObject(response);
            JSONArray resultArray = obj.getJSONArray("response");
            Date currentDate = new Date();
            for (int i = 0; i < resultArray.length(); i++) {
                if (isCancelled())
                    return;
                JSONObject resultObject = resultArray.getJSONObject(i);
                String action = resultObject.getString("action");
                String storeId = resultObject.getString("sid");
                if(action.equals("delete")) {
                    dbh.deleteStore(storeId);
                } else if(action.equals("update")) {
                    double lat = Double.parseDouble(resultObject
                            .getString("lat"));
                    double lon = Double.parseDouble(resultObject
                            .getString("lon"));
                    Store store = new Store(Double.toString(lat),
                            Double.toString(lon), resultObject.getString("name"),
                            resultObject.getString("address"), cityId);
                    store.setId(storeId);
                    store.setGroupId(resultObject.getString("group_id"));
                    dbh.updateInsertStore(store);
                }
            }
            if (isCancelled())
                return;
            dbh.setLastUpdate(cityId, currentDate);
            Log.d(this.toString(), resultArray.length() + " elements have been received from the server.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void ReadAllStoresFromDB() {
        myApp.setStores(dbh.selectAll());
    }

    public void setOnTaskFinishListener(OnTaskCompleteListener<Void> completeListener) {
        mTaskCompleteListeners.add(completeListener);
    }

    /**
     * @return the dbh
     */
    public StoresDatabaseHelper getDbh() {
        return dbh;
    }

    private void onFinish() {
        if (connectionError) {
            try {
                Toast toast = Toast.makeText(myApp.getApplicationContext(), R.string.internet_error_update_stores, Toast.LENGTH_LONG);
                toast.show();
            } catch (Exception e) {
            }
        }
        dbh.close();
    }
}