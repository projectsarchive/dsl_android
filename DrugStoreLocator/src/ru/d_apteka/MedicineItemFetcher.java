package ru.d_apteka;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Queries the web API of the server to get all the items fit the search query
 * <p>It queries the server provided in {@link MyApp#server} by default. You can override it using {@link MedicineItemFetcher#setServer(String)}
 * before calling {@link MedicineItemFetcher#getItems(java.util.ArrayList)}.</p>
 * @see MedicineItem
 * @author stas
 *
 */
public class MedicineItemFetcher {
	
	private static String server = MyApp.server;
	
	/**
	 * @see #findByName(String, int, int)
	 */
	public static ArrayList<MedicineItem> findByName(String query, int limit) throws IOException{
		return findByName(query, limit, 0);
	}
	
	/**
	 * Returns the results of querying the server for items matching <code>query</code>. 
	 * @param query Query string to make search with
	 * @param per_page Maximal number of items to be returned
	 * @param page Item number to page with
	 * @return ArrayList of {@link MedicineItem}s fetched from the server
	 * @throws IOException In case of an error while connecting to the server
	 */
	public static ArrayList<MedicineItem> findByName(String query, int per_page, int page) throws IOException{
		ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("q", query));
		params.add(new BasicNameValuePair("per_page", Integer.toString(per_page)));
		params.add(new BasicNameValuePair("page", Integer.toString(page + 1)));
		return getItems(params);
	}
	
	/**
	 * @see #findBy1DCode(String, int, int)
	 */
	public static ArrayList<MedicineItem> findBy1DCode(String code) throws IOException {
		return findBy1DCode(code, 0, 0);
	}

	/**
	 * Returns all the items with the provided code
	 * @param code Scanned 1D code
	 * @param per_page Maximal number of items to be returned
	 * @param page Item number to page with
	 * @return ArrayList of {@link MedicineItem}s fetched from the server
	 * @throws IOException In case of the error while connecting to the server
	 */
	public static ArrayList<MedicineItem> findBy1DCode(String code, int per_page, int page) throws IOException{
		ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("q", code));
        params.add(new BasicNameValuePair("type", "barcode"));
		params.add(new BasicNameValuePair("per_page", Integer.toString(per_page)));
		params.add(new BasicNameValuePair("page", Integer.toString(page + 1)));
		return getItems(params);
	}
	
	public static ArrayList<MedicineItem> getItems(ArrayList<BasicNameValuePair> initialParams) throws IOException {
		String result = "";
		String path = server + "/items/search";
		
		IWebReader wr = new WebReader();
		result = wr.readWebResource(path, initialParams);

		ArrayList<MedicineItem> resArray = new ArrayList<MedicineItem>();

		try {
			JSONObject res = new JSONObject(result);
			JSONArray jArray = res.getJSONArray("response");
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject json_data = jArray.getJSONObject(i);

				resArray.add(new MedicineItem(json_data.getInt("id"), json_data
						.getString("name")));
			}
		} catch (JSONException e) {
			Log.e(MedicineItemFetcher.class.toString(), "Error parsing data " + e.toString());
			return null;
		}
		return resArray;
	}

	/**
	 * @return the server
	 */
	public static String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	@SuppressWarnings("static-access")
	public void setServer(String server) {
		this.server = server;
	}
}
