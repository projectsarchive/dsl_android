/**
 * 
 */
package ru.d_apteka;

/**
 * Interface for passing functions for items in the main menu.
 * @author stas
 *
 */
public interface ICallable {
	public abstract void onCall(MainMenuItem item);
}
