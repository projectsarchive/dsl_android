package ru.d_apteka;

/**
 * Interface of the object that want to be informed when an async task is completed
 * @author stas
 *
 * @param <Task> Actual Async task class that we are waiting to be completed
 */
public interface OnTaskCompleteListener<Result> {
	/** Notifies about task completeness */
    void onTaskComplete(Result result);
}
