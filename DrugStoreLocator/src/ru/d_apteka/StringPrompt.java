package ru.d_apteka;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Class for prompting the user for some string input.
 * InputType may be set up using {@link #setInputType(int)}
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * 
 */
public class StringPrompt {
    private Object tag;
    private String value = "";
    private String title = "";
    private String message = "";
    private int inputType = -1;
    private StringPromptAnswerListener listener;
    private Context context;
    private EditText input;
    private AlertDialog dlg;
    private int maxLength = -1;
	private boolean mSingleLine = false;

    /**
     * 
     */
    public StringPrompt(Context context) {
        this.context = context;
    }

    /**
     * @param message
     */
    public StringPrompt(Context context, String message) {
        this.message = message;
        this.context = context;
    }

    /**
     * @param title
     * @param message
     */
    public StringPrompt(Context context, String title, String message) {
        this.title = title;
        this.message = message;
        this.context = context;
    }

    /**
     * @param title
     * @param message
     * @param value
     */
    public StringPrompt(Context context, String title, String message, String value) {
        this.title = title;
        this.message = message;
        this.value = value;
        this.context = context;
    }
    
    public void setSingleLine(boolean singleLine) {
    	mSingleLine = singleLine;
    }

    public void ask() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);

        input = new EditText(context);
        if(inputType != -1)
            input.setInputType(inputType);
        if(maxLength != -1) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(8);
            input.setFilters(FilterArray);
        }
        input.setSingleLine(mSingleLine);
        
        input.setOnEditorActionListener(new OnEditorActionListener() {

            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                if (arg1 == EditorInfo.IME_ACTION_GO
                        || arg1 == EditorInfo.IME_ACTION_DONE) {
                    positiveBtnClicked();
                }
                return false;
            }
        });
        input.setText(value);
        alert.setView(input);

        alert.setPositiveButton(R.string.dialog_positive_button,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alert.setNegativeButton(R.string.dialog_cancel_btn,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        dlg = alert.create();
        dlg.show();

        Button okBtn = dlg.getButton(AlertDialog.BUTTON_POSITIVE);
        okBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                positiveBtnClicked();
            }
        });

    }

    private void positiveBtnClicked() {
        value = input.getText().toString();
        if(listener == null || listener.onAnswer(this))
            dlg.dismiss();
    }

    /**
     * The object to identify the result. Should be set with
     * {@link #setTag(Object)}.
     * 
     * @return the tag
     */
    public Object getTag() {
        return tag;
    }

    /**
     * Any object used to identify the result
     * 
     * @param tag
     *            the tag to set
     */
    public void setTag(Object tag) {
        this.tag = tag;
    }

    /**
     * Returns the last entered value (or the default value)
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value that will be shown during the next {@link #ask()} call
     * 
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * InputType of the field on the form
     * 
     * @return the inputType
     */
    public int getInputType() {
        return inputType;
    }

    /**
     * {@link InputType} of the field on the form.
     * By default it is -1, meaning the type is not set.
     * 
     * @param inputType
     *            the inputType to set
     */
    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    /**
     * Sets the mListener of the answer event.
     * @param mListener the mListener to set
     */
    public void setListener(StringPromptAnswerListener listener) {
        this.listener = listener;
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @param maxLength Maximal length of the text entered
     */
    public void setMaxLength(int maxLength) {
        if(maxLength > 0)
            this.maxLength = maxLength;
    }

    public static abstract class StringPromptAnswerListener {
        /**
         * When the user hits the positive button or Enter this method is
         * called. If the method returns false, the dialog won't be closed.
         * 
         * @param prompt The prompt object
         * @return If the value is correct and the dialog should be closed.
         */
        public abstract boolean onAnswer(StringPrompt prompt);
    }

}