package ru.d_apteka;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class CustomAutoCompleteTextView extends AutoCompleteTextView {
	
	private OnShowDropDownListener mDropDownListener;
	
	public CustomAutoCompleteTextView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomAutoCompleteTextView(Context context) {
		super(context);
	}
	
	@Override
	protected void performFiltering(CharSequence text, int keyCode) {
		super.performFiltering(text, keyCode);
	}
	
	/**
	 * Does nothing, because we don't want to replace the text in the text view with the name of a selected item
	 */
	@Override
	protected void replaceText(CharSequence text) {
		//super.replaceText(text);
	}
	
	@Override
	public void showDropDown() {
		if(mDropDownListener != null && !mDropDownListener.onShowDropDown(this))
			return;
		super.showDropDown();
	}
	
	/**
	 * @param showDropDownListener the mDropDownListener to set
	 */
	public void setOnShowDropDownListener(OnShowDropDownListener showDropDownListener) {
		this.mDropDownListener = showDropDownListener;
	}

	public interface OnShowDropDownListener {
		boolean onShowDropDown(AutoCompleteTextView textView);
	}
}
