package ru.d_apteka;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class JSONReady {
	
	public String getJSON() {
		Class<? extends JSONReady> c = this.getClass();  
//        Log.d("Reflection", "The class " + c.getName() + " has the following fields: ");  
          
        Field[] fields = c.getDeclaredFields();  
        Method[] methods = c.getMethods();
        String getterName = "";
        String fieldRealName = "";
//        String result = "[" + this.getClass().getSimpleName() + ":{";
        String result = "{";
        boolean echo = false;
        for ( int i = 0; i < fields.length; i++ )  
        {  
        	Field field = fields[i];
        	echo = true;
			fieldRealName = getRealName(field);
        	if(!Modifier.isPublic(field.getModifiers())) {
        		getterName = "get" + fieldRealName.substring(0, 1).toUpperCase() + fieldRealName.substring(1);
        		echo = false;
        		for (Method method : methods) {
					if(method.getName().equals(getterName)) {
						echo = true;
						field.setAccessible(true);
						break;
					}
				}
        	}
        	if(!echo)
        		continue;
//        	Log.d("Reflection", methodNameReal);
        	String value = "";
        	try {
        		Object valueObj = field.get(this);
        		value = getFieldValue(valueObj, field.getName());
			} catch (Exception e) {
				e.printStackTrace();
//				continue;
			}
        	
        	String printName = fieldRealName.substring(0, 1).toLowerCase() + fieldRealName.substring(1);
        	result += "\"" + printName + "\":";
        	if(!"".equals(value)) {
        		result += value;
        	} else {
        		result += "\"\"";
        	}
        	if(i < fields.length - 1)
        		result += ",";
        	
//        	Log.d("Reflection", fieldNameReal + ": \"" + value + "\"");
        } 
//        result += "};]";
        result += "}";
//        Log.d("Reflection", result);
		return result;
	}

	private String getFieldValue(Object valueObj, String valueName) throws IllegalAccessException {
		String value;
		if(JSONReady.class.isInstance(valueObj)) {
			value = ((JSONReady)valueObj).getJSON();
		} else if(JSONReadyInterface.class.isInstance(valueObj)) {
			value = ((JSONReadyInterface)valueObj).getJSON();
		} else if(ArrayList.class.isInstance(valueObj)) {
			value = "[";
			ArrayList<?> list = (ArrayList<?>) valueObj;
			for (Object element : list) {
				value += getFieldValue(element, valueName) + ",";
			}
			value = value.substring(0, value.length() - 1);
			value += "]";
		} else {
			value = getPropertyValue(valueName, valueObj);
			if(value == null)
				value = valueObj.toString();
			value = "\"" + value + "\"";
		}
		return value;
	}

	/**
	 * Used for JSON coding - gets the encoded value of the property by its name
	 * @param valueName
	 * @return
	 */
	protected String getPropertyValue(String valueName, Object valueObj) {
		return null;
	}

	private String getRealName(Field field) {
		String name = field.getName();
		if(!Modifier.isPublic(field.getModifiers())) {
			if(name.startsWith("m") && name.length() > 1 && Character.isUpperCase(name.charAt(1))) {
				name = name.substring(1, 2).toLowerCase() + name.substring(2);
			}
		}
		return name;
	}
}
