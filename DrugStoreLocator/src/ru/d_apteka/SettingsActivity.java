/**
 * 
 */
package ru.d_apteka;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author stas
 *
 */
public class SettingsActivity extends MyActivity {
	private TextView traf;
	private MyApp app;
	
	/**
	 * {@inheritDoc}
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.settings);
		
		traf = (TextView) findViewById(R.id.traffic);
		
		app = (MyApp) getApplicationContext();
		refreshTraffic();
		
		Button btn = (Button) findViewById(R.id.refresh);
		btn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				refreshTraffic();
			}
		});
		
		Button btn2 = (Button) findViewById(R.id.show_test);
		btn2.setOnClickListener(new OnClickListener() {
            
            public void onClick(View v) {
               Intent intent = new Intent(app, QAActivity.class);
               startActivity(intent);
            }
        });
	}
	
	protected void refreshTraffic() {
		traf.setText(Double.toString(app.getTrafficStatistics().getMyTxTraffic()));
	}
}
