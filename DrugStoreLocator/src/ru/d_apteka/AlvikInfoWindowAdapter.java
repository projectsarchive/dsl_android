package ru.d_apteka;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class AlvikInfoWindowAdapter implements GoogleMap.InfoWindowAdapter{
    private View window;

    AlvikInfoWindowAdapter(LayoutInflater inflater) {
        window = inflater.inflate(R.layout.custom_balloon_overlay, null);
    }

    public View getInfoWindow(Marker marker) {
        TextView title = (TextView)window.findViewById(R.id.balloon_item_title);
        title.setText(marker.getTitle());

        TextView snippet = (TextView)window.findViewById(R.id.balloon_item_snippet);
        snippet.setText(marker.getSnippet());

        return window;
    }

    public View getInfoContents(Marker marker) {
        return null;
    }
}
