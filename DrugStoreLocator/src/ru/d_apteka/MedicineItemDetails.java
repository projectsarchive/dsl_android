/**
 * 
 */
package ru.d_apteka;

/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 *
 */
public class MedicineItemDetails {
	private String shortHtmlDescription = "";
	private String extraHtmlDescription = "";
	private MedicineItem item;
	
	/**
	 * 
	 */
	public MedicineItemDetails(MedicineItem item) {
		this.item = item;
	}
	
	/**
	 * @return the item
	 */
	public MedicineItem getItem() {
		return item;
	}
	
	public String getShortHtmlDescription() {
		return shortHtmlDescription;
	}
	public void setShortHtmlDescription(String shortHtmlDescription) {
		this.shortHtmlDescription = shortHtmlDescription;
	}
	public String getExtraHtmlDescription() {
		return extraHtmlDescription;
	}
	public void setExtraHtmlDescription(String extraHtmlDescription) {
		this.extraHtmlDescription = extraHtmlDescription;
	}
}
