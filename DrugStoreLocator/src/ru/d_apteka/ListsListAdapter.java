package ru.d_apteka;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.SwipeableHiddenView;

public class ListsListAdapter extends BaseAdapter {
	protected Context mContext;
	protected ArrayList<ItemsList> mList;
	protected UsersDatabaseHelper mHelper;
	protected LayoutInflater mInflater;
	protected boolean mShowAddNew = false;
	private HiddenQuickActionSetup mQuickActionSetup;

	public ListsListAdapter(Context context) {
		mContext = context;
		mHelper = new UsersDatabaseHelper(context);
		updateList();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setShowAddNew(boolean value) {
		mShowAddNew = value;
	}
	
	protected void updateList() {
		mList = mHelper.getUserLists();
		notifyDataSetChanged();
	}

	public int getCount() {
		if(mShowAddNew)
			return mList.size() + 1;
		else
			return mList.size();
	}

	public ItemsList getItem(int position) {
		if (position < mList.size()) {
			return mList.get(position);
		} else {
			return null;
		}
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			if (mQuickActionSetup == null)
				view = mInflater.inflate(R.layout.lists_list_adapter_item,
						parent, false);
			else {
				view = (SwipeableHiddenView) mInflater.inflate(
						R.layout.lists_list_adapter_item, parent, false);
				((SwipeableHiddenView) view)
						.setHiddenViewSetup(mQuickActionSetup);
			}
			holder = new ViewHolder();
			holder.name = (TextView) view
					.findViewById(R.id.lists_list_adapter_item_name);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		if (position < mList.size()) {
			ItemsList list = mList.get(position);
			holder.name.setText(list.getName());
		} else {
			holder.name.setText(R.string.lists_list_adapter_new_list);
		}

		return view;
	}

	private class ViewHolder {
		public TextView name;
	}
	
	public void setQuickActionSetup(HiddenQuickActionSetup quickActionSetup) {
		mQuickActionSetup = quickActionSetup;
	}

}
