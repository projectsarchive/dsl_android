package ru.d_apteka;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.SwipeableHiddenView;

public class ListsAdapter extends BaseAdapter {
	private ItemsList mList;
	private LayoutInflater mInflater;
	private HiddenQuickActionSetup mQuickActionSetup;

	// private Context mContext;

	public ListsAdapter(Context context, ItemsList list) {
		mList = list;
		// mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return mList.getItems().size();
	}

	public MedicineItem getItem(int position) {
		ArrayList<MedicineItem> items = mList.getItems();
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ArrayList<MedicineItem> items = mList.getItems();
		ViewHolder holder;

		if (convertView == null) {
			if (mQuickActionSetup == null)
				convertView = mInflater.inflate(R.layout.lists_adapter_item,
						parent, false);
			else {
				convertView = (SwipeableHiddenView) mInflater.inflate(
						R.layout.lists_adapter_item, parent, false);
				((SwipeableHiddenView) convertView)
						.setHiddenViewSetup(mQuickActionSetup);
			}
			holder = new ViewHolder();
			holder.name = (TextView) convertView
					.findViewById(R.id.lists_item_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MedicineItem medicineItem = items.get(position);
		holder.name.setText(medicineItem.getName());
		return convertView;
	}

	private class ViewHolder {
		public TextView name;
	}

	public void setQuickActionSetup(HiddenQuickActionSetup quickActionSetup) {
		mQuickActionSetup = quickActionSetup;
	}
}
