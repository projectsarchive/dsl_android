/**
 * 
 */
package ru.d_apteka;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * @author stas
 *
 */
public class ScanCodeActivity extends MyActivity {
	/**
	 * {@inheritDoc}
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setTitle(getString(R.string.scan_code_title));
        setContentView(R.layout.scan_code);
		
		Bundle extras = getIntent().getExtras();

		if (extras == null) {
			finish();
		}
		
		@SuppressWarnings("unchecked")
		ArrayList<MedicineItem> items = (ArrayList<MedicineItem>) extras.getSerializable("items");
		
		ListView list = (ListView) findViewById(R.id.codeItemsListView);
		list.setAdapter(new ArrayAdapter<MedicineItem>(this, R.layout.availability_item,
                R.id.availability_text1, items));
		list.setOnItemClickListener(itemClickListener);
	}
	
	private OnItemClickListener itemClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			try {
				MedicineItem item = (MedicineItem) parent.getItemAtPosition(position);
				Intent intent = new Intent(view.getContext(), AvailabilityListActivity.class);
				intent.putExtra("showDetailsBtn", true);
				intent.putExtra("item", item);
				startActivity(intent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
}
