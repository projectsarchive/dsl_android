package ru.d_apteka;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

public class StoreInformation extends JSONReady {
	private Store mStore;
	private ArrayList<WorkingDay> mWorkingDays;
	
	public StoreInformation(Store store) {
		mStore = store;
	}

	/**
	 * @return the store
	 */
	public Store getStore() {
		return mStore;
	}

	/**
	 * @param store the store to set
	 */
	public void setStore(Store store) {
		this.mStore = store;
	}
	
	/**
	 * @return the workingDays
	 */
	public ArrayList<WorkingDay> getWorkingDays() {
		return mWorkingDays;
	}

	/**
	 * @param workingDays the workingDays to set
	 */
	public void setWorkingDays(ArrayList<WorkingDay> workingDays) {
		this.mWorkingDays = workingDays;
	}

	public static class WorkingDay extends JSONReady {
		public ArrayList<WorkingHour> hours;
		public int dayId;
        public boolean currentDay;
		
		public WorkingDay() {
			hours = new ArrayList<StoreInformation.WorkingDay.WorkingHour>();
		}
		
		@Override
		protected String getPropertyValue(String valueName, Object valueObj) {
			if(valueName.equals("dayId") && dayId < 8) {
				int day = dayId + 1;
				if(day == 8)
					day = 1;
				String name = new DateFormatSymbols().getWeekdays()[day];
				name = name.substring(0, 1).toUpperCase() + name.substring(1);
				return name;
			} else if(valueName.equals("currentDay")) {
                return currentDay ? "1" : "";
            }
			return super.getPropertyValue(valueName, valueObj);
		}
		
		public static class WorkingHour extends JSONReady {
			public int from;
			public int to;
			
			public String getHour(int hourInt) {
				int hours = hourInt / 60;
//				new DateFormatSymbols().getWeekdays()[]
				String time = String.format("%02d:%02d", hours, hourInt - hours * 60);
				return time;
			}
			
			@Override
			protected String getPropertyValue(String valueName, Object valueObj) {
				try {
					return getHour((Integer)valueObj);
				} catch (Exception e) {
					return super.getPropertyValue(valueName, valueObj);
				}
			}
		}
	}
}
