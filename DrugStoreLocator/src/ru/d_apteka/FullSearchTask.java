/**
 * 
 */
package ru.d_apteka;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Fills in the {@link SearchActivity#listSearch} with the results fetched from
 * the server
 * 
 * @author stas
 * 
 */
public class FullSearchTask extends
		AsyncTask<DynamicTaskParams<String>, Void, ArrayList<MedicineItem>> {
	private OnTaskCompleteListener<ArrayList<MedicineItem>> mTaskCompleteListener;
	private Context context;
	private boolean internetError = false;
	private boolean cancelled = false;
	
	public FullSearchTask(Context context) {
		this.context = context;
	}

	@Override
	protected ArrayList<MedicineItem> doInBackground(DynamicTaskParams<String>... params) {
		try {
			return MedicineItemFetcher.findByName(params[0].getParams(),
					params[0].getPageSize(),
					params[0].getPage());
		} catch (IOException e) {
			internetError = true;
			return null;
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		cancelled = true;
		OnTaskCompleted(null);
	}
	
	/**
	 * @return the cancelled
	 */
	public boolean isReallyCancelled() {
		return cancelled;
	}

	@Override
	protected void onPostExecute(ArrayList<MedicineItem> result) {
		super.onPostExecute(result);

		OnTaskCompleted(result);
	}

	/**
	 * Sets the {@link OnTaskCompleteListener} for the current task
	 * 
	 * @param completeListener
	 *            The complete listener to be set
	 */
	public void setOnTaskCompleteListener(
			OnTaskCompleteListener<ArrayList<MedicineItem>> completeListener) {
		this.mTaskCompleteListener = completeListener;
	}

	private void OnTaskCompleted(ArrayList<MedicineItem> result) {
		if (mTaskCompleteListener != null)
			mTaskCompleteListener.onTaskComplete(result);
		if(internetError) {
			try {
				Toast toast = Toast.makeText(context, R.string.internet_error_full_search, Toast.LENGTH_LONG);
				toast.show();
			} catch(Exception e) {}
		}
	}
	
}