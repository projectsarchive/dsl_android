package ru.d_apteka;

import ru.d_apteka.ListsEditor.ListsEditedListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import de.viktorreiser.toolbox.util.AndroidUtils;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup;
import de.viktorreiser.toolbox.widget.HiddenQuickActionSetup.OnQuickActionListener;


public class ListsListActivity extends MyActivity implements OnQuickActionListener {
	private ListsListAdapter mAdapter;
	private ListView mListView;
	// Quick Action <==========================================================
	
	private static final class QuickAction {
		public static final int RENAME = 1;
		public static final int DELETE = 2;
	}

	private HiddenQuickActionSetup mQuickActionSetup;
	
	// Quick Action >==========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.lists_list_activity);
		super.onCreate(savedInstanceState);
		setTitle(getString(R.string.lists_list_activity_my_lists));
		
		mAdapter = new ListsListAdapter(this);
		mListView = (ListView) findViewById(R.id.lists_list_list_view);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ItemsList list = (ItemsList) arg0.getItemAtPosition(arg2);
				
				Intent intent = new Intent(mActivity, ListsActivity.class);
				intent.putExtra("listId", list.getId());
				startActivity(intent);
			}
		});
		// Quick Action <======================================================
		setupQuickAction();

		mAdapter.setQuickActionSetup(mQuickActionSetup);
		mListView.setAdapter(mAdapter);
		// Quick Action >======================================================
	}
	
	// Quick Action <==========================================================
	
	/**
	 * React on quick action click.
	 */
	public void onQuickAction(AdapterView<?> parent, View view, int position, int quickActionId) {
		final ItemsList list = mAdapter.getItem(position);
		switch (quickActionId) {
		case QuickAction.RENAME:
			ListsEditor editor = new ListsEditor(mActivity);
			editor.setListToEdit(list);
			editor.setListsAddedListener(new ListsEditedListener() {
				
				@Override
				public void onListEdited(ItemsList list, MedicineItem item) {
					mAdapter.updateList();
				}
			});
			editor.editList();
			break;

		case QuickAction.DELETE:
			new AlertDialog.Builder(mActivity)
			.setTitle(R.string.lists_list_delete_title)
			.setMessage(String.format(getString(R.string.lists_list_delete_message), list.getName()))
			.setPositiveButton(R.string.lists_list_delete_yes, new OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					UsersDatabaseHelper oHelper = new UsersDatabaseHelper(mActivity);
					oHelper.removeList(list);
					mAdapter.updateList();
				}
			})
			.setNegativeButton(R.string.lists_list_delete_no, null)
			.show();
			break;
		}
	}

	/**
	 * Create a global quick action setup.
	 */
	private void setupQuickAction() {
		mQuickActionSetup = new HiddenQuickActionSetup(this);
		mQuickActionSetup.setOnQuickActionListener(this);

		// a nice cubic ease animation
		mQuickActionSetup.setOpenAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				v -= 1;
				return v * v * v + 1;
			}
		});
		mQuickActionSetup.setCloseAnimation(new Interpolator() {
			public float getInterpolation(float v) {
				return v * v * v;
			}
		});

		int imageSize = AndroidUtils.dipToPixel(this, 40);

		mQuickActionSetup.setBackgroundResource(R.drawable.ui_qa_background);
		mQuickActionSetup.setImageSize(imageSize, imageSize);
		mQuickActionSetup.setAnimationSpeed(700);
		mQuickActionSetup.setStartOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setStopOffset(AndroidUtils.dipToPixel(this, 20));
		mQuickActionSetup.setSwipeOnLongClick(true);

		mQuickActionSetup.addAction(QuickAction.RENAME,
				R.string.search_activity_qa_rename, R.drawable.ui_qa_rename);
		mQuickActionSetup.addAction(QuickAction.DELETE,
				R.string.search_activity_qa_delete_list, R.drawable.ui_qa_delete_list);
	}

	// Quick Action >==========================================================
}
